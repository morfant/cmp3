// var ruleset = [0, 1, 0, 1, 1, 0, 1, 0].reverse() // rule 90
// var ruleset = [1, 1, 0, 1, 1, 1, 1, 0].reverse() // rule 222
// var ruleset = [1, 0, 1, 1, 1, 1, 1, 0].reverse() // rule 190
// var ruleset = [0, 0, 0, 1, 1, 1, 1, 0].reverse() // rule 30
// var ruleset = [0, 1, 1, 0, 1, 1, 1, 0].reverse() // rule 110
var ruleset = []
var w = 1
var current // Array
var generation = 0

// set ruleset as decimal
var RULE_DEC = 98819

function setup() {
    createCanvas(800, 800)
    background(100)

    ruleset = decToRuleset(RULE_DEC)
    console.log("Ruleset: " + ruleset + "(" + RULE_DEC + ")")

    current = new Array(width / w).fill(0)
    current[current.length / 2] = 1
    noStroke()
}

function draw() {
    var next = new Array(current.length).fill(0)
    for (var i = 1; i < current.length - 1; i++) {
        var left = current[i - 1]
        var middle = current[i]
        var right = current[i + 1]

        var newState = rule(left, middle, right)
        // console.log(newState)

        next[i] = newState
    }

    generation++

    current = next

    for (var i = 0; i < current.length; i++) {
        if (current[i] == 1) {
            fill(0)
        } else if (current[i] == 0) {
            fill(255)
        } else {
            fill(255, 0, 0) // display error
        }

        rect(i * w, w * generation, w, w)
    }
}

function rule(l, m, r) {
    var bin = "" + l + m + r
    var index = parseInt(bin, 2)

    return ruleset[index]
}

function decToRuleset(dec) {
    // ex 110

    var bin = dec.toString(2) // convert dec to binary
    // console.log(bin)
    var result = []
    var binArr = bin.split("") // make bin as Array (convert from string to Array)

    if (bin.length < 8) {
        for (var i = 0; i < 8 - bin.length; i++) {
            binArr.unshift(0) // fill 0 at the begining of binArr
        }
    }

    for (var i = binArr.length - 1; i >= 0; i--) {
        result.push(parseInt(binArr[i])) // insert reversely
    }

    return result
}
