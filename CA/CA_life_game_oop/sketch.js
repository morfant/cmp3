var w = 10
var current = []
var column, row // number of

class Cell {

    constructor(x, y, w, state) {
        this.x = x
        this.y = y
        this.w = w
        this.previous = state
        this.current = 0
        this.age = 0
    }

    // set x(arg) {
    //     this.x = arg
    // }

    // set y(arg) {
    //     this.y = arg
    // }

    // set setCurrent(arg) {
    //     this._state_current = arg
    // }

    // set setPrevious(arg) {
    //     this._state_previous = arg
    // }

    // get getPrevious() {
    //     return this._state_previous
    // }

    // get getCurrent() {
    //     return this._state_current
    // }

}


function setup() {
    createCanvas(1600, 800);
    background(100)
    frameRate(3)
    // noStroke()

    column = width / w
    row = height / w

    // init current randomly
    for (var i = 0; i < column; i++) {
        current[i] = []
        for (var j = 0; j < row; j++) {
            if (i != 0 && j != 0 && i != column - 1 && j != row - 1) {
                current[i][j] = new Cell(i * w, j * w, w, round(random(0, 0.52)))
            } else {
                current[i][j] = new Cell(i * w, j * w, w, 0)
            }
        }
    }

}

function draw() {

    generate()
    display()

}

function generate() {

    for (var x = 1; x < column - 1; x++) { // caution: the range
        for (var y = 1; y < row - 1; y++) {

            // make current as previous
            // current[x][y].previous = current[x][y].current

            var neighbors = 0 // number of neighbors

            for (var i = -1; i <= 1; i++) {
                for (var j = -1; j <= 1; j++) {
                    if (current[x + i][y + j].previous == 1) {
                        neighbors++;
                    }
                }
            }

            neighbors = neighbors - current[x][y].previous
            // console.log("neighbor: " + neighbors)

            // console.log(x + " / " + y)
            var newState = rule(current[x][y].previous, neighbors)
            // console.log("newState: " + newState)

            current[x][y].current = newState

            if (current[x][y].current == 1) {
                current[x][y].age++
            } else {
                current[x][y].age = 0
            }
            // console.log("current[x][y]: " + current[x][y].current)

        }
    }


    for (var x = 1; x < column - 1; x++) { // caution: the range
        for (var y = 1; y < row - 1; y++) {

            // make current as previous
            current[x][y].previous = current[x][y].current
        }
    }



}

function display() {
    for (var x = 0; x < column; x++) {
        for (var y = 0; y < row; y++) {

            if (current[x][y].current == 1) {
                fill(0)
                // fill(0, current[x][y].age * 40)

                // if (current[x][y].previous == 0) {
                //     fill(0, 0, 255)
                // }

            } else if (current[x][y].current == 0) {
                fill(255)

                // if (current[x][y].previous == 1) {
                //     fill(255, 0, 0, 80)
                // }

            } else {
                fill(0, 255, 0) // display error
            }
            rect(current[x][y].x, current[x][y].y, current[x][y].w, current[x][y].w)
        }
    }
}

function rule(self, neighbors) {

    if (self == 1) {
        if (neighbors <= 1 || neighbors >= 4) {
            return 0
        }
    } else if (self == 0) {
        if (neighbors >= 3) {
            return 1
        }
    } else {
        // check display error
        return -1
    }

    return self
}