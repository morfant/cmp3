var w = 10
var cells = []
var column, row // number of

class Cell {

    constructor(x, y, w, state) {
        this.x = x
        this.y = y
        this.w = w
        this.previous = state
        this.current = 0
        this.age = 0
    }
}


class HexCell {
    constructor(x, y, r, state, id) {

        this.x = x
        this.y = y
        this.r = r

        // state
        this.current = 0
        this.previous = state
        this.id = id
        this.neighbors = 0
    }

    draw(r, g, b) {

        stroke(0)
        fill(r, g, b)
        hexgon(this.x, this.y, this.r)

        // text
        // fill(0)
        // textSize(20)
        // text(this.id, this.x - 5, this.y)


    }

    drawNeighbors() {

        // text
        fill(255)
        textSize(16)
        text(this.neighbors, this.x - 5, this.y)

    }
}


function setup() {
    createCanvas(1000, 800);
    background(100)
    frameRate(4)
    // noStroke()

    column = width / w - 40
    row = height / w
    var side = w
    var rand_max = 0.51

    // init current randomly
    // https://www.redblobgames.com/grids/hexagons/#coordinates-doubled
    // double-height

    for (var x = 0; x < column; x++) {
        cells[x] = []
        for (var y = 0; y < row; y += 2) { // 2 step

            if (x % 2 == 1) {
                cells[x][y + 1] = new HexCell(x * side * 1.5, y / 2 * side * sqrt(3) + (side * sqrt(3) / 2), side, round(random(0, rand_max)), x + "-" + (y + 1))
            } else {
                cells[x][y] = new HexCell(x * side * 1.5, y / 2 * side * sqrt(3), side, round(random(0, rand_max)), x + "-" + y)
            }
        }
    }

    // set property of border blocks
    for (var x = 0; x < column; x++) {
        for (var y = 0; y < row; y++) {
            if (x < 2 || y < 2 || x >= (column - 2) || y >= (row - 2)) {
                if (cells[x][y]) {
                    cells[x][y].previous = 0
                    cells[x][y].neighbors = -1
                }
            }
        }
    }

    // console.log(cells)

}

function draw() {

    generate()

    push()
    translate(50, 50)
    display()
    pop()

}

function generate() {

    var neighbor_directions = [
        [-1, -1], [0, -2], [1, -1],
        [1, 1], [0, 2], [-1, 1]
    ]

    for (var x = 2; x < column - 2; x++) { // caution: the range
        for (var y = 2; y < row - 2; y++) {

            if (cells[x][y]) { // check is it exist

                var neighbors = 0 // number of neighbors

                neighbor_directions.forEach(d => {
                    var dx = d[0]
                    var dy = d[1]
                    if (cells[x + dx][y + dy].previous == 1) {
                        neighbors++;
                    }
                })

                cells[x][y].neighbors = neighbors
                var newState = rule(cells[x][y].previous, neighbors)

                cells[x][y].current = newState

            }
        }

    }

    // make current as previous : important!
    for (var x = 2; x < column - 2; x++) { // caution: the range
        for (var y = 2; y < row - 2; y++) {

            if (cells[x][y]) { // check is it exist

                cells[x][y].previous = cells[x][y].current
            }
        }
    }



}


function display() {

    for (var x = 0; x < column; x++) {
        for (var y = 0; y < row; y++) {

            if (cells[x][y]) {
                if (cells[x][y].current == 1) {
                    cells[x][y].draw(0, 0, 0)
                    // cells[x][y].drawNeighbors()
                } else if (cells[x][y].current == 0) {
                    cells[x][y].draw(255, 255, 255)
                    // cells[x][y].drawNeighbors()
                } else {
                    cells[x][y].draw(100, 100, 100)
                }

            }
        }
    }
}


function rule(self, neighbors) {

    if (self == 1) {
        if (neighbors <= 1 || neighbors >= 3) {
            return 0
        }
    } else if (self == 0) {
        if (neighbors >= 2) {
            return 1
        }
    } else {
        // check display error
        return -1
    }

    return self
}

function polygon(x, y, radius, npoints) {
    let angle = TWO_PI / npoints; // radian

    beginShape();
    for (let a = 0; a < TWO_PI; a += angle) {
        let sx = x + cos(a) * radius;
        let sy = y + sin(a) * radius;
        vertex(sx, sy);
    }
    endShape(CLOSE);
}

function hexgon(x, y, r) {
    polygon(x, y, r, 6)
}