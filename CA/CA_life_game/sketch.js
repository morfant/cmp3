    var ruleset = []
    var w = 80
    var current // 2D Array (cell)
    var column, row // number of

    function setup() {
        createCanvas(1600, 800);
        background(100)
        frameRate(3)

        column = width / w
        row = height / w
        current = new Array(column).fill(0).map(x => new Array(row).fill(0)) // make 2D array

        // init current randomly
        for (var x = 0; x < column; x++) {
            for (var y = 0; y < row; y++) {
                current[x][y] = round(random()) // 0 or 1
            }
        }

        console.log("Init values: " + current)

    }

    function draw() {

        var next = new Array(column).fill(0).map(x => new Array(row).fill(0)) // make 2D array

        for (var x = 1; x < column - 1; x++) { // caution: the range
            for (var y = 1; y < row - 1; y++) {

                var neighbors = 0 // number of neighbors

                for (var i = -1; i <= 1; i++) {
                    for (var j = -1; j <= 1; j++) {
                        if (current[x + i][y + j] == 1) {
                            neighbors++;
                        }
                    }
                }

                neighbors = neighbors - current[x][y]

                var newState = rule(current[x][y], neighbors)

                next[x][y] = newState
            }
        }

        // make current as next
        current = next

        // display
        for (var x = 0; x < column; x++) {
            for (var y = 0; y < row; y++) {

                if (current[x][y] == 1) {
                    fill(0)
                } else if (current[x][y] == 0) {
                    fill(255)
                } else {
                    fill(255, 0, 0) // display error
                }

                rect(x * w, y * w, w, w)
            }

        }
    }

    function rule(self, neighbors) {

        if (self == 1) {
            if (neighbors <= 1 || neighbors >= 4) {
                return 0
            }
        } else if (self == 0) {
            if (neighbors >= 3) {
                return 1
            }
        } else {
            // display error
            return -1
        }

        return self
    }