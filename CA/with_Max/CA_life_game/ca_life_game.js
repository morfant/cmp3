autowatch = 1
inlets = 1
outlets = 2


var cells = []; // Array
var w = 50
var h = 50
var width, height
var column, row // number of cells
var rand_max = 0.9


function msg_float(num) {
    rand_max = num
}

function size(w, h) {
    width = w
    height = h
    post("w / h", width, height, "\n")
}

function setup() {

    max.clearmaxwindow()

    if (width && height) {
        column = width / w
        row = height / h
        post(column, row, "\n")
    } else {
        post("set variable \'width\', \'height\' first")
        return
    }

    // init 2D array 
    for (var x = 0; x < column; x++) {
        cells[x] = [] // Array of arrays, make 2D array
        for (var y = 0; y < row; y++) {
            // cells[x][y] = Math.round(rrand(0, rand_max))
            cells[x][y] = Math.round(Math.random())
        }
    }

}

function draw() {
    generate()
    display()
}


function generate() {

    // init 2D array next
    var next = []
    for (var x = 0; x < column; x++) {
        next[x] = [] // Array of arrays, make 2D array
        for (var y = 0; y < row; y++) {
            next[x][y] = 0
        }
    }

    for (var x = 1; x < column - 1; x++) {
        for (var y = 1; y < row - 1; y++) {

            // calculate number of neighbors
            var neighbors = 0

            for (var i = -1; i <= 1; i++) {
                for (var j = -1; j <= 1; j++) {
                    if (cells[x + i][y + j] === 1) {
                        neighbors++
                    }
                }
            }

            // subtract itself from sum
            neighbors -= cells[x][y]

            // apply rule to make next generation
            var newState = rule(cells[x][y], neighbors)
            // console.log(newState)

            next[x][y] = newState

        }
    }

    // make next as current
    cells = next
}

function display() {

    var color = []
    for (var x = 0; x < column; x++) {
        for (var y = 0; y < row; y++) {

            if (cells[x][y] === 1) {
                color = [0, 0, 0]
            } else if (cells[x][y] === 0) {
                color = [255, 255, 255]
            } else {
                color = [255, 0, 0]
            }

            // It is possible to include type of js array as element of Max list
            // left, top, right, bottom, r, g, b => color
            outlet(0, x * w, y * h, (x + 1) * w, (y + 1) * h, color)

            // Show grid with framerect
            outlet(1, x * w, y * h, (x + 1) * w, (y + 1) * h, 0, 0, 0)

        }
    }

}


function rule(self, neighbors) {

    if (self === 1) {
        if (neighbors <= 1 || neighbors > 3) {
            return 0
        }

    } else if (self === 0) {
        if (neighbors == 3) {
            return 1
        }

    } else {
        // Error check
        return -1
    }

    return self
}


function rrand(min, max) {
    return Math.random() * (max - min) + min; // Math.random returns [0, 1)
}
