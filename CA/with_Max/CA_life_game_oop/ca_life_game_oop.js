autowatch = 1
inlets = 1
outlets = 2

var cells = []; // Array
var w = 80
var width, height
var column, row // number of cells
var rand_max = 0.6

// include class file
include('Cell.js')

function msg_float(num) {
    rand_max = num
}

function size(w, h) {
    width = w
    height = h
    post("w / h", width, height, "\n")
}

function setup() {

    max.clearmaxwindow()

    if (width && height) {
        column = width / w
        row = height / w
        post("column: ", column, " row: ", row, "\n")
    } else {
        post("set variable \'width\', \'height\' first")
        return
    }

    // init current randomly
    for (var i = 0; i < column; i++) {
        cells[i] = []
        for (var j = 0; j < row; j++) {
            if (i != 0 && j != 0 && i != column - 1 && j != row - 1) {
                cells[i][j] = new Cell(i * w, j * w, w, Math.round(rrand(0, rand_max)))
            } else {
                cells[i][j] = new Cell(i * w, j * w, w, 0)
            }
        }
    }

}

function draw() {
    generate()
    display()
}

function generate() {

    for (var x = 1; x < column - 1; x++) { // caution: the range
        for (var y = 1; y < row - 1; y++) {

            var neighbors = 0 // number of neighbors

            for (var i = -1; i <= 1; i++) {
                for (var j = -1; j <= 1; j++) {
                    if (cells[x + i][y + j].previous === 1) {
                        neighbors++;
                    }
                }
            }

            neighbors = neighbors - cells[x][y].previous
            // post("neighbor: " + neighbors, "\n")

            var newState = rule(cells[x][y].previous, neighbors)
            // post("newState: " + newState, "\n")

            cells[x][y].current = newState

        }
    }


    // make current as previous
    for (var x = 0; x < column; x++) { // caution: the range
        for (var y = 0; y < row; y++) {

            cells[x][y].previous = cells[x][y].current
        }
    }

}

function display() {

    var color = []

    for (var x = 0; x < column; x++) {
        for (var y = 0; y < row; y++) {

            if (cells[x][y].previous === 1) {
                color = [0, 0, 0]
            } else if (cells[x][y].previous === 0) {
                color = [255, 255, 255]
            } else {
                color = [255, 0, 0]
            }

            // It is possible to include type of js array as element of Max list
            // left, top, right, bottom, r, g, b => color
            outlet(0,
                cells[x][y].x, cells[x][y].y,
                cells[x][y].x + w, cells[x][y].y + w,
                color)

            // Show grid with framerect

            outlet(1,
                cells[x][y].x, cells[x][y].y,
                cells[x][y].x + w, cells[x][y].y + w,
                0, 0, 0)

        }
    }

}


function rule(self, neighbors) {

    if (self === 1) {
        if (neighbors <= 1 || neighbors > 3) {
            return 0
        }

    } else if (self === 0) {
        if (neighbors >= 2) {
            return 1
        }

    } else {
        // Error check
        return -1
    }

    return self
}


function rrand(min, max) {
    return Math.random() * (max - min) + min; // Math.random returns [0, 1)
}
