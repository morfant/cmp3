function Cell(x, y, w, state) {

    this.x = x
    this.y = y
    this.w = w
    this.previous = state
    this.current = 0
    this.age = 0
}


/*

// class syntax is not supported in Max yet

class Cell {

    constructor(x, y, w, state) {
        this.x = x
        this.y = y
        this.w = w
        this.previous = state
        this.current = 0
        this.age = 0
    }
}
*/