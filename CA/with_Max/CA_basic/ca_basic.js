autowatch = 1
inlets = 1
outlets = 1


var width, height
var ruleset = []
var w = 1
var current = []; // Array
var generation = 0

// set ruleset as decimal
var RULE_DEC = 30

var color = []


function bang(b) {
    draw()
}

function msg_int(num) {
    // change ruleset
    RULE_DEC = num
    ruleset = decToRuleset(RULE_DEC)
    post("Ruleset: " + ruleset + "(" + RULE_DEC + ")", "\n")
}

function size(w, h) {
    width = w
    height = h
    post("w / h", width, height, "\n")
}

function setup() {

    msg_int(RULE_DEC)

    current = new Array(width / w)

    // init current as 0
    for (var i = 0; i < current.length; i++) {
        current[i] = 0
    }

    // init value
    current[current.length / 2] = 1

    generation = 0

}

function generate() {
    var next = new Array(current.length)

    // init
    for (var i = 0; i < next.length; i++) {
        next[i] = 0
    }

    // apply rule to make next generation
    for (var i = 1; i < current.length - 1; i++) {

        var left = current[i - 1]
        var middle = current[i]
        var right = current[i + 1]

        var newState = rule(left, middle, right)
        // console.log(newState)

        next[i] = newState
    }

    generation++

    // make next as current
    current = next

}

function display() {

    for (var i = 0; i < current.length; i++) {
        if (current[i] == 1) {
            color = [0, 0, 0]
        } else if (current[i] == 0) {
            color = [255, 255, 255]
        } else {
            color = [255, 0, 0]
        }

        // post(0, i * w, w * generation, w, w, color, "\n")
        // left, top, right, bottom, r, g, b
        outlet(0, i * w, w * generation, i * w + w, w * generation + w,
            color)
    }
}

function draw() {

    generate()
    display()

}

function rule(l, m, r) {
    var bin = "" + l + m + r
    var index = parseInt(bin, 2)

    return ruleset[index]
}

function decToRuleset(dec) { // ex 110

    var bin = dec.toString(2) // convert dec to binary
    var result = []
    var binArr = bin.split("") // make bin as Array (convert from string to Array)

    if (bin.length < 8) {
        for (var i = 0; i < 8 - bin.length; i++) {
            binArr.unshift(0) // fill 0 at the begining of binArr
        }
    }

    for (var i = binArr.length - 1; i >= 0; i--) {
        result.push(parseInt(binArr[i])) // insert reversely
    }

    return result
}