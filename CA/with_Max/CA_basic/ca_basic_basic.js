autowatch = 1
inlets = 1
outlets = 1


var width, height
var ruleset = [0, 0, 0, 0, 1, 0, 0, 0]
var oneGridWidth = 1
var current = [] // current generation
var generation = 0
var color = []

function size(w, h) {
    width = w
    height = h
    post("w / h", width, height, "\n")
}

function setup() {

    current = new Array(width / w)

    // init current as 0
    for (var i = 0; i < current.length; i++) {
        current[i] = 0
    }

    // init value
    current = ruleset
    generation = 0

}

function setRule(a, b, c, d, e, f, g, h) {
    ruleset = []
    ruleset.push(a)
    ruleset.push(b)
    ruleset.push(c)
    ruleset.push(d)
    ruleset.push(e)
    ruleset.push(f)
    ruleset.push(g)
    ruleset.push(h)
    post("RULE SET: " + ruleset, "\n")
}

function bang() {
    generate()
    display()
}


function generate() {
    var next = []

    // init
    for (var i = 0; i < current.length; i++) {
        next[i] = 0
    }

    // apply rule to make next generation
    for (var i = 1; i < current.length - 1; i++) {

        var left = current[i - 1]
        var middle = current[i]
        var right = current[i + 1]

        var newState = rule(left, middle, right)
        // console.log(newState)

        next[i] = newState
    }

    generation++

    // make next as current
    current = next

}

function display() {

    for (var gridIdx = 0; gridIdx < current.length; gridIdx++) {
        if (current[gridIdx] == 1) {
            color = [0, 0, 0]
        } else if (current[gridIdx] == 0) {
            color = [255, 255, 255]
        } else {
            color = [255, 0, 0] // error
        }

        // left, top, right, bottom, r, g, b
        outlet(0,
            gridIdx * oneGridWidth,
            generation * oneGridWidth,
            (gridIdx + 1) * oneGridWidth,
            (generation + 1) * oneGridWidth,
            color)
    }
}

function rule(l, m, r) {
    var bin = "" + l + m + r
    var index = parseInt(bin, 2) // 2진수(bin)를 10진수(dec)로 바꾸어 준다. ex) 011 ==> 3, 101 ==> 5

    return ruleset[index]
}
