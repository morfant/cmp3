autowatch = 1
outlets = 8


var width, height

// var x, y
// var vx, vy
// var r = 20 // radius

var ballColor = [0, 0, 0]

var nBall = 4
var balls = []


function Ball(x, y, vx, vy, r) {
    this.x = x
    this.y = y
    this.vx = vx
    this.vy = vy
    this.r = r
    this.inArea = false
    this.color = [0, 0, 0]

    this.isInArea = function () {
        if (this.x < width - 100
            && this.x > 100
            && this.y < height - 100
            && this.y > 100
        ) {
            this.inArea = true
            this.color = [20, 200, 100]

            // this.vx = this.vx + Math.random() * 3
            // this.vy = this.vy + Math.random() * 3

            this.r = Math.random() * 50 + 10



        } else {
            this.inArea = false
            this.color = [0, 0, 0]
        }

    }


    this.update = function () {
        this.x += this.vx
        this.y += this.vy

        var l = this.x - this.r / 2
        var r = this.x + this.r / 2
        var t = this.y - this.r / 2

        var vel = this.r * 2
        var dur = (Math.abs(this.vx) + Math.abs(this.vy)) * 200

        if (this.x + this.r / 2 >= width) {
            this.vx *= -1
            var note = Math.round(this.y / height * 127)
            // sendNote(4, note, vel, dur)
        }
        if (this.x - this.r / 2 <= 0) {
            this.vx *= -1
            var note = Math.round(this.y / height * 127)
            // sendNote(5, note, vel, dur)
        }

        if (this.y + this.r / 2 >= height) {
            this.vy *= -1
            var note = Math.round(this.x / width * 127)
            // sendNote(6, note, vel, dur)
        }

        if (this.y - this.r / 2 <= 0) {
            this.vy *= -1
            var note = Math.round(this.x / width * 127)
            // sendNote(7, note, vel, dur)
        }

    }

}

function size(w, h) {
    width = w
    height = h
}


function setup() {

    balls = []
    for (var i = 0; i < nBall; i++) {
        var x = 10 + Math.random() * (width - 20)
        var y = 10 + Math.random() * (height - 20)

        var vx = Math.random() * 3
        var vy = Math.random() * 3

        var r = Math.random() * 20 + 10

        balls.push(new Ball(x, y, vx, vy, r))
    }

}


function update() {

    for (var i = 0; i < balls.length; i++) {
        balls[i].update()
        balls[i].isInArea()
    }

}


function sendNote(out, note, vel, dur) {

    var noteveldur = [note, vel, dur]
    outlet(out, noteveldur)
}


function display() {

    outlet(0, "clear")
    outlet(0, "framerect", 100, 100, 400, 400, 0, 0, 0)
    for (var i = 0; i < balls.length; i++) {
        outlet(i, "paintoval", balls[i].x - balls[i].r / 2, balls[i].y - balls[i].r / 2,
            balls[i].x + balls[i].r / 2, balls[i].y + balls[i].r / 2, balls[i].color)
    }
}


function bang() {
    update()
    display()
}
