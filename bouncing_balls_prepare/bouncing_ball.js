autowatch = 1
outlets = 2


var width, height

var x, y
var r = 20 // radius

var vx, vy

var ballColor = [0, 0, 0]


function size(w, h) {
    width = w
    height = h
}


function setup() {

    x = Math.random() * width
    y = Math.random() * height

    vx = Math.random() * 3
    vy = Math.random() * 5

    post("vx / vy: ", vx, " / ", vy, "\n")

}


function update() {
    x += vx
    y += vy

    if (x + r / 2 >= width) {
        vx *= -1

        var note = Math.round(y / height * 127)
        sendNote(note)
    }
    if (x - r / 2 <= 0) {
        vx *= -1
        var note = Math.round(y / height * 127)
        sendNote(note)
    }

    if (y + r / 2 >= height || y - r / 2 <= 0) {
        vy *= -1
        var note = Math.round(x / width * 127)
        sendNote(note)
    }


}


function sendNote(note) {

    outlet(1, note)
}


function display() {
    outlet(0, "clear")
    outlet(0, "paintoval", x - r / 2, y - r / 2, x + r / 2, y + r / 2, ballColor)
}


function bang() {
    update()
    display()
}

