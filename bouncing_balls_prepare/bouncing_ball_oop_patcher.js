autowatch = 1
outlets = 0


var width, height

// var x, y
// var vx, vy
// var r = 20 // radius

var ballColor = [0, 0, 0]

var nBall = 1
var balls = []

var p
var lcd

var obj_makenote = []
var obj_noteout = []


function clearObjs() {
    if (obj_makenote.length) {
        for (var i = obj_makenote.length - 1; i >= 0; i--) {
            p.remove(obj_makenote[i])
            p.remove(obj_noteout[i])
        }
    }

    obj_makenote = []
    obj_noteout = []

    balls = []

}


function msg_int(val) {

    clearObjs()

    nBall = val

    if (p) {
        for (var i = 0; i < nBall; i++) {
            var objmakenote = p.newdefault(600, 300, "makenote")
            var objnoteout = p.newdefault(600, 320, "noteout")

            p.connect(objmakenote, 0, objnoteout, 0)
            p.connect(objmakenote, 1, objnoteout, 1)

            obj_makenote.push(objmakenote)
            obj_noteout.push(objnoteout)
        }
    }

    post(obj_noteout.length)


}


function size(w, h) {
    width = w
    height = h
}


function setup() {


    p = this.patcher

    lcd = p.getnamed("lcd")

    balls = []
    for (var i = 0; i < nBall; i++) {
        var r = Math.random() * 20 + 10

        var x = r + Math.random() * (width - 2 * r)
        var y = r + Math.random() * (height - 2 * r)

        var vx = (Math.random() - 0.5) * 3
        var vy = (Math.random() - 0.5) * 3


        balls.push(new Ball(i, x, y, vx, vy, r))
    }

}


function update() {

    for (var i = 0; i < balls.length; i++) {
        balls[i].update()
    }

}


function sendNote(id, note, vel, dur) {

    var noteveldur = [note, vel, dur]

    obj_makenote[id].message(note, vel, dur)
    // obj_makenote[id].message(noteveldur)

}


function display() {

    // outlet(0, "clear")
    lcd.message("clear")

    for (var i = 0; i < balls.length; i++) {
        lcd.message("paintoval", balls[i].x - balls[i].r / 2, balls[i].y - balls[i].r / 2,
            balls[i].x + balls[i].r / 2, balls[i].y + balls[i].r / 2, balls[i].color)
    }
}


function bang() {
    update()
    display()
}


// Ball class
function Ball(id, x, y, vx, vy, r) {
    this.id = id
    this.x = x
    this.y = y
    this.vx = vx
    this.vy = vy
    this.r = r
    this.color = [0, 0, 0]

    this.update = function () {
        this.x += this.vx
        this.y += this.vy

        var l = this.x - this.r / 2
        var r = this.x + this.r / 2
        var t = this.y - this.r / 2

        var vel = this.r * 5
        var dur = (Math.abs(this.vx) + Math.abs(this.vy)) * 200

        if (this.x + this.r / 2 >= width) {
            this.vx *= -1
            var note = Math.round(this.y / height * 127)
            sendNote(this.id, note, vel, dur)
            this.color = [100, 200, 200]
        } else if (this.x - this.r / 2 <= 0) {
            this.vx *= -1
            var note = Math.round(this.y / height * 127)
            sendNote(this.id, note, vel, dur)
            this.color = [100, 200, 200]
        } else if (this.y + this.r / 2 >= height) {
            this.vy *= -1
            var note = Math.round(this.x / width * 127)
            sendNote(this.id, note, vel, dur)
            this.color = [100, 200, 200]
        } else if (this.y - this.r / 2 <= 0) {
            this.vy *= -1
            var note = Math.round(this.x / width * 127)
            sendNote(this.id, note, vel, dur)
            this.color = [100, 200, 200]
        } else {
            this.color = [0, 0, 0]
        }

    }

}

