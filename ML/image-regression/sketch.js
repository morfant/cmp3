let mobilenet
let video
let value = 0
let predictor
let slider
let addButton
let trainButton

function modelReady() {
    console.log("Model is ready!!")
    mobilenet.predict(gotResults)
}

function videoReady() {
    console.log("video is ready!!")
}

function gotResults(error, result) {
    if (error) {
        console.error(error)
    } else {
        // console.log(results)
        value = result

        predictor.predict(gotResults)
    }
}

// function imageReady() {
//     image(penguin, 0, 0, width, height)
// }

function setup() {
    createCanvas(640, 520)
    video = createCapture(VIDEO)
    video.hide()
    background(0)
    mobilenet = ml5.featureExtractor("MobileNet", modelReady)
    predictor = mobilenet.regression(video, videoReady)

    slider = createSlider(0, 1, 0.5, 0.01)
    slider.input(function() {
        console.log(slider.value())
    })

    trainButton = createButton("train")
    trainButton.mousePressed(function() {
        predictor.train(whileTraining)
    })

    addButton = createButton("add example image")
    addButton.mousePressed(function() {
        predictor.addImage(slider.value())
    })
}

function whileTraining(loss) {
    if (loss == null) {
        console.log("Training Complete")
        predictor.predict(gotResults)
    }
}

function draw() {
    background(0)
    image(video, 0, 0)
    fill(255)
    textSize(20)
    text(value, 10, height - 10)
}
