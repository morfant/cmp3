let mobilenet
let video
let label

function modelReady() {
    console.log("Model is ready!!")
    mobilenet.predict(gotResults)
}

function gotResults(error, results) {
    if (error) {
        console.error(error)
    } else {
        // console.log(results)
        label = results[0].label
        let confidence = results[0].confidence

        mobilenet.predict(gotResults)
    }
}

// function imageReady() {
//     image(penguin, 0, 0, width, height)
// }

function setup() {
    createCanvas(640, 520)
    video = createCapture(VIDEO)
    video.hide()
    background(0)
    mobilenet = ml5.imageClassifier("MobileNet", video, modelReady)
}

function draw() {
    background(0)
    image(video, 0, 0)
    fill(255)
    textSize(20)
    text(label, 10, height - 10)
}
