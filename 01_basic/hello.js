autowatch = 1;
outlets = 2;

var mtk_number = 12;
post("\n", mtk_number);

function sum(a, b) {
    var result = a + b;
    return result;
}

function bang() {
    var r = sum(10, 20)
    outlet(0, "hello")
    outlet(1, r)
}

function msg_int(number_i) {
    post("int()\n");
    post(number_i);
}

function msg_float(number_f) {
    post("float()\n");
    post(number_f);
}