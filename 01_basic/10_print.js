autowatch = 1

var nx = 10
var ny = 10
var count_x = 0
var count_y = 0

function setup() {
    count_x = 0
    count_y = 0
}

function coin() {
    var dx = 200 / nx // 20
    var dy = 200 / ny // 20
    var r = Math.random()
    // post(r, "\n")

    if (r < 0.5) {
        post("Front", "\n")
        drawLine(
            count_x * dx, count_y * dy,
            (count_x + 1) * dx, (count_y + 1) * dy
        ) //front
    } else {
        post("Back", "\n")
        drawLine(
            (count_x + 1) * dx, count_y * dy,
            count_x * dx, (count_y + 1) * dy
        ) //back
    }

    count_x = count_x + 1
    // post(count_x)

    if (count_x >= nx) {
        count_y = count_y + 1
        count_x = 0
    }
}

function drawLine(sx, sy, ex, ey) {
    outlet(0, "linesegment", sx, sy, ex, ey)
}

