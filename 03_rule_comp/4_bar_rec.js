autowatch = 1
outlets = 12
inlets = 1

var bar, beat

var notes = []
var currentNote = 0
var length1 = 4
var length2 = 3
var currentLength = 0
var noteInterval = [1, 2, 2, 2, 1, 2, 2]
var noteCounter = 0
var prevNoteTime = 0

var recorder = []
var track = []


function bang() {

    if (beat >= 16) {
        beat = 0; bar = 0;
    }

    beat++ // beat = beat + 1
    if ((beat - 1) % 4 == 0) {
        bar++
    }

    makeNewNote()

    // post(recorder.length, "\n")
    if (recorder.length > 1) {
        playNote()
    }


    post("beat: ", beat, " / bar: ", bar, "\n")

}

function setup() {
    max.clearmaxwindow()
    currentLength = length1
    beat = 0
    bar = 0

    recorder = []
    track = []
    currentNote = 40
    noteCounter = 0
    prevNoteTime = 0

}

function toggleLength() {
    if (currentLength == length1) {
        currentLength = length2
    } else if (currentLength == length2) {
        currentLength = length1
    }

    post("currentLength: ", currentLength, "\n")
}


function loopSubtract(a, b) {

    // post(a, b, "\n")
    var r = 0
    if (a < b) {
        r = b - a
    } else {
        r = (b + 16) - a
    }
    // post("r: ", r, "\n")
    return r
}

function jsObject(prop) {

    var aaa = {
        pitch: 66, // 1 pair
        dur: 100,
        vel: 120,
    }

    // obj.prop == obj[prop]
    post(aaa.vel, "\n")
}

function makeNewNote() {

    // post("makeNewNote()\n")

    if (loopSubtract(prevNoteTime, (beat - 1)) % currentLength == 0) {
        var note = {
            startBeat: beat,
            endBeat: beat + currentLength,
            pitch: currentNote,
        }

        // post("curlength", currentLength, "\n")
        // post("push", note, "\n")
        track.push(note)
        outlet(11, note.pitch, 80)

        currentNote = currentNote + noteInterval[noteCounter]
        noteCounter++
        if (noteCounter >= noteInterval.length) {
            noteCounter = 0
        }

        if (hasSameStartNote() == true) {
            toggleLength()
        }
        prevNoteTime = beat - 1

    }

    if ((beat - 1) == 0) {
        if (track.length > 0) {
            recorder.push(track)
            // post("recorder: ", recorder, "\n")
            track = []
        }
    }
    // post("track: ", track, "\n")

}


function forLoopTest() {

    var a = [1, 2, 3, 4, 5, 0, 2]
    post(a.length, "\n")

    // noteInterval 
    for (var i = 0; i < noteInterval.length; i = i + 1) {
        // post("i: ", i, "\n")
        post(noteInterval[i], "\n")
    }
}


function playNote() {

    var outnotes = []
    for (var i = 0; i < recorder.length; i++) {
        for (var j = 0; j < recorder[i].length; j++) {
            if (recorder[i][j].startBeat == beat) {
                outnotes.push(recorder[i][j].pitch)
            }
        }
    }

    if (outnotes.length > 0) {
        for (var i = 0; i < outnotes.length; i++) {
            outlet(i, outnotes[i], 80)
        }
    }
}

function hasSameStartNote() {

    var result = false

    for (var i = 0; i < recorder.length; i++) {
        for (var j = 0; j < recorder[i].length; j++) {
            if (recorder[i][j].startBeat == beat) {
                result = true
                break
            }
        }
        if (result == true) break
    }

    return result

}