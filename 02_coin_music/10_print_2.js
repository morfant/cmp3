autowatch = 1
outlets = 2

var nx = 10
var ny = 10
var count_x = 0
var count_y = 0

var col_red = 0

var note = 60
var velocity = 64

// test.push(1)
// test ==> [1]
// test.push(10)
// test ==> [1, 10, 11, 1,]

var currentDir = []
var prevDir = []

// read
// prevDir[0] = 1
// prevDir[1] = -1
// prevDir[5] = 1

// write
// prevDir[8] = 1



function setup() {
    count_x = 0
    count_y = 0
}


function looptest() {
    for (var i = 0; i < ny; i++) {
        for (var j = 0; j < nx; j++) {
            coin()
        }
    }
}

function coin() {
    var dx = 200 / nx // 20
    var dy = 200 / ny // 20
    var r = Math.random()
    // post(r, "\n")

    if (r < 0.5) {

        post("Front", "\n")
        // post("Front\n")
        // post("Front" + "\n")

        drawLine( // '\'
            count_x * dx, count_y * dy,
            (count_x + 1) * dx, (count_y + 1) * dy
        ) //front
        currentDir.push(-1)
        sendNote(-1)
        // SinOsc.ar(440, 0.2, 0.3)
    } else {
        post("Back", "\n")
        drawLine( // '/'
            (count_x + 1) * dx, count_y * dy,
            count_x * dx, (count_y + 1) * dy
        ) //back
        currentDir.push(1)
        sendNote(1)
    }

    // new grid
    count_x = count_x + 1 // 0 ~ inf

    // new line 
    if (count_x >= nx) {
        count_y = count_y + 1
        count_x = 0

        col_red = Math.round(Math.random() * 255)
        prevDir = currentDir.slice() // clone
        currentDir = []
        if (count_y >= ny) {
            count_y = 0
            outlet(0, "clear")
            prevDir = []
            currentDir = []
        }

    }
}

function arrayCopyTest() {

    post("arrayCopyTest()\n")

    var a = [1, 1, 1, 1]
    var b = []

    // b = a
    b = a.slice() // clone

    post("b1: ", b, "\n")

    a[2] = 100

    // 이런 부분 없는데
    // b[2] = 100

    // 1, 1, 100, 1
    post("b2: ", b, "\n")

}

function drawLine(sx, sy, ex, ey) {
    outlet(0, "linesegment", sx, sy, ex, ey, col_red, 0, 0)
}

function sendNote(d) {

    post("sendNote()\n")
    // post(d, "\n")

    // if (checkConnect() == true) {
    //     velocity = 120
    // } else {
    //     velocity = 64
    // }

    if (d == 1) {
        note = 64
        // if (checkConnect == true) // 1
        if (checkConnect() == true) { // 2
            note = -1
        }
    } else if (d == -1) {
        note = 45
        if (checkConnect() == true) { // 2
            note = -4
        }
    }

    outlet(1, note, velocity)
}


function checkConnect() {

    // count_x: 0 ~ 9

    var isConnected = false // true - false
    if (currentDir[count_x] == 1) {

        // [-1, 1, 1, 1, -1, -1, 1, 1, 1, -1]
        if (prevDir[count_x] == -1 || prevDir[count_x + 1] == 1) {
            isConnected = true
        }

    } else if (currentDir[count_x] == -1) {
        // 숙제
        if (prevDir[count_x - 1] == -1 || prevDir[count_x] == 1) {
            isConnected = true
        }
    }
    return isConnected

}

