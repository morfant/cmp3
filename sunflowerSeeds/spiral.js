autowatch = 1


var a, b, theta
var r
var x, y

var color = [0, 0, 0]

var width, height

function setup() {
    r = 0

    x = 200
    y = 200

    a = 2
    b = 5
    theta = 0

    width = 400
    height = 400

}


function bang() {
    display()

}


function degToRad(d) {
    return d * (Math.PI / 180)
}



function display() {
    var radTheta = degToRad(theta)
    r = a + b * radTheta
    x = r * Math.cos(radTheta) + width / 2
    y = r * Math.sin(radTheta) + height / 2


    var rr = 2
    outlet(0, "frameoval", x - rr, y - rr, x + rr, y + rr, color)

    theta = theta - 1

}