autowatch = 1
outlets = 2

var n = 0
var c = 2
var d = 1
var rSeed = 4
var seedColor = [0, 0, 0]

var octaveIntervals = [55, 110, 220, 440, 880]

function bang() {
    draw()
}


function setup() {
    n = 0
}

function draw() {

    var theta = n * 137.6
    var r = c * Math.sqrt(n)

    var x = 250 + 2 * r * Math.cos(degToRad(theta))
    var y = 250 + 2 * r * Math.sin(degToRad(theta))

    outlet(0, "paintoval", x - rSeed / 2, y - rSeed / 2, x + rSeed / 2, y + rSeed / 2, seedColor)
    makeNote(r, theta)

    n = n + d
    if (n >= 1000) {
        d = -1
    } else if (n <= 0) {
        d = 1
    }

}

function degToRad(d) {
    return d * (Math.PI / 180)
}

function makeNote(r, theta) {
    // post("r: ", r)
    var _theta = theta % 360
    post("theta: ", theta, " / ", _theta)
    post()
    var areaIdx = 0
    var freq = 0
    var radius = 2 * Math.PI * r

    if (r < 10) {
        areaIdx = 0
    } else if (r < 20) {
        areaIdx = 1
    } else if (r < 30) {
        areaIdx = 2
    } else if (r < 40) {
        areaIdx = 3
    } else {
        areaIdx = 4
    }

    // freq = _theta / 360 * octaveIntervals[areaIdx] + octaveIntervals[areaIdx]
    freq = _theta / 360 * 880
    // freq = _theta / 360 * radius + radius
    outlet(1, freq)

}