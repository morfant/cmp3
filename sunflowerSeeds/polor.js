autowatch = 1


var width, height
var x, y
var theta
var r = 1
var color = [0, 0, 0]
var k, n, d



function bang() {
    display()
}

function setup() {
    width = 400
    height = 400
    x = 0
    y = 0
    theta = 0

    outlet(0, "linesegment", 0, height / 2, width, height / 2)
    outlet(0, "linesegment", width / 2, 0, width / 2, height)

    n = 3
    d = 7
    k = n / d

}


function display() {


    x = r * Math.cos(degToRad(theta))
    y = r * Math.sin(degToRad(theta))


    var ovalRadius = 2
    outlet(0, "paintoval",
        width / 2 + x - ovalRadius, height / 2 + y - ovalRadius,
        width / 2 + x + ovalRadius, height / 2 + y + ovalRadius, color)

    post("theta: ", theta % 360)
    post()

    theta = theta + 1
    r = 100 * Math.sin(degToRad(theta * k))

    post("r: ", r)
    post()

    if (theta % 360 == 0) {
        // outlet(0, "clear")
    }

    // outlet(0, "linesegment", width / 2, height / 2, width / 2 + x, height / 2 + y)


}


function degToRad(deg) {

    return deg * (Math.PI / 180)

}