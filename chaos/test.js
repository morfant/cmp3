autowatch = 1
outlets = 5

var width, height
var color = [0, 0, 0]
var r // 2.0 ~ 4.0
var p // 0.0 ~ 1.0
var pl // p of last year
var step_r, mul_p
var count = 0
var noteCount = 0
var points = []

var p, msg_index, lcd_playbar

function size(w, h) {
    width = w
    height = h
}

function setup() {
    count = 0
    step_r = (4.0 - 2.0) / width // r은 2.0 ~ 4.0 사이의 값인데, width 번동안 얼만큼 씩 증가 시켜야 4.0 - 2.0 이 증가 될 것인가
    mul_p = height / (1.0 - 0.0) // p는 0. ~ 1.0 사이의 값인데, 여기에 어떤 수를 곱해야 0 ~ height 사이 값으로 표현 되는가
    r = 2.0 // 2.0 ~ 4.0

    points = []

    outlet(0, "clear")

    p = this.patcher

    msg_index = p.getnamed("msg_index")
    lcd_playbar = p.getnamed("lcd_playbar")
}

function showNotes() {

    post("length of points: ", points.length, "\n")

    for (var i = 0; i < points.length; i++) {
        var notes = points[i]

        for (var j = 0; j < notes.length; j++) {
            // post(notes[j])
            // post()
        }
    }

}

function goto(n) {
    noteCount = n
}

function soundPos(n) {

}

function sound() {
    playBar(noteCount)
    playNote(noteCount)
    noteCount++

    if (noteCount >= points.length) {
        noteCount = 0
    }
}

function playBar(n) {
    outlet(2, "clear")
    outlet(2, "linesegment", n, 0, n, 100, 255, 0, 0)

    outlet(4, n)
    var lcd_left = lcd_playbar.rect[0]
    var lcd_bottom = lcd_playbar.rect[3]
    msg_index.message("position", lcd_left + n, lcd_bottom + 5)

}

function playNote(n) {
    var notes = points[n]
    outlet(3, notes)
}

function display() {

    p = 0.2 // r이 바뀔 때마다, 초기 개체수(p)를 reset 해 주어야 함.

    var notes = []
    for (var year = 0; year < 200; year++) {

        var rad = 1

        pl = p * r * (1 - p)

        if (year > 100) {
            var x, y
            x = count
            y = height - (pl * mul_p)
            outlet(0, "paintoval", x - rad, y - rad, x + rad, y + rad, color)

            var note = (height - y)
            note = Math.round(note / height * 127)
            // 중복 체크가 필요 : logistic map이 안정화 되어서 같은 위치에 점이 여러번 찍힐 수 있기 때문에.
            if (notes.contains(note) == false) {
                notes.push(note)
            }
        }
        p = pl
    }

    points.push(notes)

    r = r + step_r
    count++

    // 종료 조건
    if (count > width) {
        outlet(1, 0)
        showNotes()
    }
}




Array.prototype.contains = function (e) {

    if (this.length == 0) {
        return false
    } else {
        for (var i = 0; i < this.length; i++) {
            if (e == this[i]) {
                return true
            }
        }
        return false
    }

}

