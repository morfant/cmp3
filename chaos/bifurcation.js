autowatch = 1
outlets = 5

var width, height
var color = [0, 0, 0]

var r, p
var minR = 2.0, maxR = 4.0
var minP = 0.0, maxP = 1.0
var dr, dp
var x, y

var points = []
var notes = []

var t = [0.1, 1.23, 4.56, 2]

var count = 0

var p, playBarLcd, index

function size(w, h) {
    width = w
    height = h
}

function setup() {
    r = 2.0
    p = 0.5
    dr = (maxR - minR) / width
    dp = (maxP - minP) / height
    x = 0
    y = 0
    points = []
    notes = []
    count = 0

    p = this.patcher

    index = p.getnamed("msg_index")
    playBarLcd = p.getnamed("lcd_playbar")
}

function bang() {
    display()
}

function logistic(r) {
    return r * p * (1.0 - p)
}


function oneYear(year) {

    var len = 1
    r = minR + x * dr
    if (r >= 3.99) {
        outlet(1, 0)
    }
    p = logistic(r);

    var y = height - (p - minP) / dp
    if (year > 100) {
        outlet(0, "paintrect", x - len, y - len, x + len, y + len, color)

        if (notes.contains(y) == 0) {
            notes.push(height - y)
            post(notes)
            post()
        }

    }

}

function test() {
    // post(t.contains(n))
    // post()

    // post(index.attr_names)
    post(playBarLcd.valid)
    post()
    index.message("patching_position", 100, 200)
}

function display() {
    p = 0.5

    notes = []
    for (var i = 0; i < 105; i++) {
        oneYear(i)
    }

    points.push(notes)
    // post("points==========")
    // post()
    x++
}

function showNotes() {

    for (var i = 0; i < points.length; i++) {
        post("points[", i, "]")
        post()
        var p = points[i]
        for (var j = 0; j < p.length; j++) {
            post(p[j])
            post()
        }
    }


}

function soundPos(n) {

    playBar(n)
    outlet(2, points[n])

}

function sound() {

    playBar(count)
    outlet(2, points[count])
    count++

    if (count >= width) {
        outlet(1, 0)
        count = width - 1
    }

}

function playBar(n) {

    outlet(3, "clear")
    outlet(3, "linesegment", n - 0.5, 0, n + 0.5, 100, [255, 0, 0])
    outlet(4, n)

    var lcd_left = playBarLcd.rect[0]
    var lcd_bottom = playBarLcd.rect[3]

    index.message("position", lcd_left + n, lcd_bottom + 5)

}

function goto(n) {
    count = n
}



Array.prototype.contains = function (needle) {

    if (this.length == 0) {
        return false
    } else {
        for (var i = 0; i < this.length; i++) {
            if (this[i] == needle) {
                return true
            }
        }
        return false
    }

}