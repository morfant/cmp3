autowatch = 1

var width, height
var xRes = 20
var p, r
var year = 0


function size(w, h) {
    width = w
    height = h
    // post(width, "/", height, "\n")
}

function setup(_p, _r) {
    p = _p
    r = _r
    year = 0
    xRes = 20 // graph of 60 years
}

function oneYear() {
    var x, y, new_x, new_y

    x = year * xRes
    y = height - (p * height)

    // predict population
    p = p * r * (1 - p)

    // increase year
    year++

    new_x = year * xRes
    new_y = height - (p * height)

    outlet(0, "linesegment", x, y, new_x, new_y)
}

function bang() {
    oneYear()
}