autowatch = 1
outlets = 2

var width = 500
var height = 500
var x = 0, y = 0
var color = [0, 0, 0]
var octaves = [0, 55, 110, 220, 440, 880]

var n = 0
var c = 10
var r = 50
var theta = 0

var basic_angle = 137.5

function bang() {
    display()
}


function setup() {
    x = 0
    y = 0
    theta = 0
    n = 0
}

function msg_float(f) {
    basic_angle = f
}


function degToRad(deg) { // degrees => radians
    return Math.PI / 180 * deg
}

function display() {

    theta = n * basic_angle
    r = c * Math.sqrt(n)

    x = r * Math.cos(-1 * degToRad(theta))
    y = r * Math.sin(-1 * degToRad(theta))

    var oval_r = 4
    outlet(0,
        "paintoval",
        width / 2 + x - oval_r, height / 2 + y - oval_r,
        width / 2 + x + oval_r, height / 2 + y + oval_r, color)


    makeNote(theta, r)

    n = n + 1

}

function makeNote(deg, r) {

    // theta : degrees 


    // 11 % 10 ==> 1(나머지)
    // 11 / 10 ==> 1(몫)
    // 405 % 360 ===> 45

    var _theta = deg % 360
    // var freq = 0

    // if (r < 10) {
    //     // 0 ~ 55
    //     freq = octaves[0] + _theta / 360 * octaves[1]
    // } else if (r < 20) {
    //     // 56 ~ 110
    //     freq = octaves[1] + _theta / 360 * octaves[2]
    // } else if (r < 30) {
    //     // 111 ~ 220
    //     freq = octaves[2] + _theta / 360 * octaves[3]
    // } else if (r < 40) {
    //     // 221 ~ 440
    //     freq = octaves[3] + _theta / 360 * octaves[4]
    // } else {
    //     // 441 ~ 880
    //     freq = octaves[4] + _theta / 360 * octaves[5]
    // }


    var freq = _theta / 360 * 880
    outlet(1, freq)

}