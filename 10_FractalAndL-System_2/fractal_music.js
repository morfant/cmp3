autowatch = 1
outlets = 2


var axiom = "F-F-F-F"

var width, height
var x, y, direction, step, step_angle
var current = axiom
var stack = []

var len = 0

var lines = []
var count = 0
var prevDir = -1
var prevEy = -1

function size(w, h) {
    width = w
    height = h
}

function reset() {
    stack = []
    current = axiom
    count = 0
}

function setup() {
    outlet(0, "clear")
    step = 10
    direction = -90
    // x = width / 2
    x = 100
    y = height - 100

    step_angle = 90

}

function generate() {
    setup()
    lines = []

    var next = ""

    for (var i = 0; i < current.length; i++) {
        var char = current[i]

        if (char == "F") {
            next = next + "F-F+F+FF-F-F+F"
            // next = next + "FF"
            // next = next + "FF-[-F+F+F]+[+F-F-F]"
            // next = next + "F[+F]F[-F][F]"
            // next = next + "FF-F-F-F-FF"
            // next = next + "FF-F-F-F-F-F+F"
        } else if (char == "X") {
            // next = next + "F-[[X]+X]+F[+FX]-X"
            next = next + "F[+X][-X]FX"
        } else {
            next = next + char // bypass
        }
    }

    current = next

    post(current, "\n")

}



function turtle(lineAdd) {
    setup()

    for (var i = 0; i < current.length; i++) {

        var char = current[i]

        if (char == "F") {
            new_x = x + step * Math.cos(degToRad(direction))
            new_y = y + step * Math.sin(degToRad(direction))

            outlet(0, "linesegment", x, y, new_x, new_y, 0, 0, 0)

            if (lineAdd == false) {
                var dir = Math.abs(direction)
                // if (dir == 0 || dir == 180 || dir == 360) {
                if (dir % 180 == 0) {
                    if (dir == prevDir && new_y == prevEy) {
                        lines[lines.length - 1].len = step * 2
                    } else {
                        lines.push({ bx: x, by: y, ex: new_x, ey: new_y, dir: dir, len: step })
                    }
                    prevDir = dir
                    prevEy = new_y
                }
            }

            x = new_x
            y = new_y

        } else if (char == "f") {
            new_x = x + step * Math.cos(degToRad(direction))
            new_y = y + step * Math.sin(degToRad(direction))

            // outlet(0, "linesegment", x, y, new_x, new_y)

            x = new_x
            y = new_y

        } else if (char == "+") {
            direction = direction - step_angle
            len = 0
        } else if (char == "-") {
            direction = direction + step_angle
            len = 0
        } else if (char == "[") {
            var obj = { posx: x, posy: y, dir: direction }
            stack.push(obj)
        } else if (char == "]") {
            var obj = stack.pop()
            x = obj.posx
            y = obj.posy
            direction = obj.dir
        } else {

        }
    }
}


function lineSearch(idx) {

    // turtle(true)
    if (idx < 0) {
        idx = lines.length - 1 - Math.abs(idx)
    }
    if (idx >= 0) {
        outlet(0, "linesegment", lines[idx].bx, lines[idx].by, lines[idx].ex, lines[idx].ey, 255, 0, 0)
    }

    getLines(idx)
}

function getLines(i) {

    if (i < 0) { i = lines.length - 1 - Math.abs(i) }
    outlet(1, (height - lines[i].ey) / height * 127, lines[i].len)
}

function degToRad(deg) {
    return deg / 180 * Math.PI
}

function play() {

    outlet(1, idx)
}

function seq() {
    count++

    if (count >= lines.length) {
        count = 0
    }

    lineSearch(count)

}