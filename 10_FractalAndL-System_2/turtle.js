autowatch = 1
outlets = 2

var step = 8 //radius
var angle = 90 //theta
var direction = -90
var x = 200, y = 400
var axiom = "X"
var current = axiom
var stack = []
var lines = []
var count = 0

function reset() {
    current = axiom
    lines = []
    stack = []
    count = 0
}

function degToRad(deg) {
    return (deg / 180) * Math.PI
}

function generate() {
    var next = ""
    for (var i = 0; i < current.length; i++) {
        if (current[i] == "F") {
            next = next + "FF" // rewriting
        } else if (current[i] == "X") {
            next = next + "F-[[X]+X]+F[+FX]-X"
        } else {
            next = next + current[i]
        }
    }
    current = next
}

function draw() {
    for (var i = 0; i < current.length; i++) {
        turtle(current[i])
    }
}

function bang() {
    outlet(1, lines[count].posy)
    count++
    if (count >= lines.length) {
        count = 0
    }
}

function turtle(char) {
    if (char == "F") {
        new_x = x + Math.cos(degToRad(direction)) * step
        new_y = y + Math.sin(degToRad(direction)) * step
        outlet(0, "linesegment", x, y, new_x, new_y)

        if (direction == 0 || direction == 180) {
            var line = { posx: x, posy: y }
            lines.push(line)
        }
        x = new_x
        y = new_y
    } else if (char == "f") {
        new_x = x + Math.cos(degToRad(direction)) * step
        new_y = y + Math.sin(degToRad(direction)) * step
        x = new_x
        y = new_y
    } else if (char == "+") {
        direction = direction - angle
    } else if (char == "-") {
        direction = direction + angle
    } else if (char == "[") { // push
        var obj = { posx: x, posy: y, dir: direction }
        stack.push(obj)
    } else if (char == "]") { // pop
        var obj = stack.pop()
        x = obj.posx
        y = obj.posy
        direction = obj.dir
    }
}