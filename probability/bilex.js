autowatch = 1
inlets = 1
outlets = 2

function bang(b) {
    bilex(0.02)
    gauss(0.2, 0)
}

function bilex(lamda) {
    var r = Math.random() // random range [0.0, 1.0)
    var u, s
    var result = undefined

    u = 2 * r

    if ((u == 0) | (u == 2)) return // just exit this function if u is 0 or 2

    if (u > 1) {
        u = 2 - u
        s = -1
    } else {
        s = 1
    }

    result = (s * Math.log(u)) / lamda
    // post(result, "\n")

    outlet(0, result)
    return result
}

function gauss(sigma, mu) {
    var result = undefined
    var r = Math.random()
    var k
    var N = 512
    var halfN = N / 2
    var scale = 1
    var sum = 0
    for (var k = 1; k <= N; k++) {
        sum += Math.random()
    }

    result = sigma * scale * (sum - halfN) + mu
    outlet(1, result)
    return result
}
