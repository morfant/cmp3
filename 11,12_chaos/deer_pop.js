autowatch = 1

var height, width
var year = 0

var p, r // 개체수, 재생산률
var color = [0, 0, 0]
var xRes = 80 // x의 간격을 조정하기 위한 변수


// lcd의 사이즈 msg를 통해 js 코드 상의 width, height 값을 바꾸는 함수
function size(w, h) {
    width = w
    height = h
}

// 초기화를 위한 함수
function setup(_p, _r) {
    p = _p
    r = _r
    year = 0
}

// 다음 해의 개체수를 구하고 그리기 위한 함수
function oneYear() {
    var x, y, new_x, new_y // 지역변수로 사용해도 문제가 없을 때는 지역변수를 사용하는 것이 더 좋습니다!

    // 올해
    x = year * xRes
    y = p * height

    // p, year 갱신
    p = p * r * (1.0 - p)
    year++

    // 내년
    new_x = year * xRes
    new_y = p * height

    outlet(0, "linesegment", x, y, new_x, new_y, color)

}