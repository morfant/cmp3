autowatch = 1
outlets = 4


var width, height
var p // 0.0 ~ 1.0
var r // 2.0 ~ 4.0
var step_r
var count, playCount
var points = []
var maxpatcher, index, lcd_playbar

function size(w, h) {
    width = w
    height = h
}

// 여러가지 값들의 초기화
function setup() {
    p = 0.2
    r = 2.0
    count = 0
    playCount = 0
    step_r = (4.0 - 2.0) / width

    // max 오브젝트를 js에서 다루기 위한 준비.
    maxpatcher = this.patcher
    index = maxpatcher.getnamed("msg_index")
    lcd_playbar = maxpatcher.getnamed("lcd_playbar")
}

// 특정 위치로 playbar를 옮긴다.
function goto(n) {
    playCount = n
}

function playBar(n) {

    // playbar 그리기
    outlet(2, "clear")
    outlet(2, "linesegment", n, 0, n, 60, 255, 0, 0)

    // max의 msg 오브젝트를 playbar 위치에 따라 이동시키기.
    outlet(3, n)

    var lcd_left = lcd_playbar.rect[0]
    var lcd_bottom = lcd_playbar.rect[3]

    index.message("position", lcd_left + n, lcd_bottom + 10)

}


function sound() {

    playBar(playCount)

    outlet(1, points[playCount])
    playCount++

    // width보다 커지려고 하면 다시 0 으로.
    if (playCount >= width) {
        playCount = 0
    }

}


function display() {

    p = 0.2 // p 초기화
    var notes = []

    for (var year = 0; year < 200; year++) {

        // p, year 갱신
        p = p * r * (1.0 - p)

        if (year > 100) {

            var color = [0, 0, 0]
            var x, y, rad = 0.5

            x = count
            y = (1.0 - p) * height

            outlet(0, "paintoval", x - rad, y - rad, x + rad, y + rad, color)

            // 1) 0 ~ 127 사이 값으로 변환
            var midinote = Math.round((1.0 - (y / height)) * 127)

            // 2) 중복되지 않는 값만 push!
            // Array가 사용할 수 있는 함수들의 리스트에 새로운 것을 추가할 수 있다.
            if (notes.includes(midinote) == false) {
                notes.push(midinote)
            }
        }
    }

    // 각 x 좌표에 대한 notes들을 points 에 넣는다.
    points.push(notes)

    count = count + 1
    r = r + step_r
}


// midinote가 제대로 들어갔는지 확인
function showNotes() {

    for (var i = 0; i < points.length; i++) {
        var notes = points[i]
        for (var j = 0; j < notes.length; j++) {
            post(notes[j])
            post()
        }
        post("-------------------------------")
        post()
    }

}


Array.prototype.includes = function (e) {

    // Array가 비어있다면 바로 false 를 return 하고 종료 한다.
    if (this.length == 0) {
        return false
    }

    for (var i = 0; i < this.length; i++) {
        if (this[i] == e) {
            return true
            // 함수 안에서 return을 하면 즉시 함수 종료! [0, 1, 2, 3, 4 ,5, 6]
            // 이후의 element에 대해서는 체크할 수 없다. 
        }
    }

    return false
}

/*
숙제 예제

// [0.1, 0.52, 1.5] ==> [0, 1, 2]
Array.prototype.round = function () {
    // array 안의 모든 숫자들을 반올림한다.
}
*/