autowatch = 1
inlets = 1
outlets = 3

var cells = []
var width = 500
var height = 500
var gridWidth = 10
var gridHeight = 10


function bang() {
    generate()
}

function setup() {
    max.clearmaxwindow()

    for (var i = 0; i < (height / gridHeight); i++) {
        cells[i] = []
        for (var j = 0; j < (width / gridWidth); j++) {
            cells[i][j] = Math.round(Math.random())
        }
    }

    // post(cells)
    // post(cells[9][9], "\n")

}

function rule(me, n) {

    if (me == 1) {
        if (n >= 4 || n <= 1) {
            // 다음 세대에서, 죽는다
            return 0
        }

    } else if (me == 0) {
        if (n == 3) {
            // 다음 세대애서, 살아난다
            return 1
        }

    } else {
        post("error\n")
        return -1

    }

    // 위의 조건 어디에도 해당하지 않을때는 현상유지. 
    return me

}

function generate() {

    var next = []
    for (var i = 0; i < cells.length; i++) {
        next[i] = []
        for (var j = 0; j < cells[i].length; j++) {
            next[i][j] = 0
        }
    }


    for (var i = 1; i < cells.length - 1; i++) {
        for (var k = 1; k < cells[i].length - 1; k++) {

            var numNeighbour =
                cells[i - 1][k - 1] + cells[i - 1][k] + cells[i - 1][k + 1] +
                cells[i][k - 1] + cells[i][k + 1] +
                cells[i + 1][k - 1] + cells[i + 1][k] + cells[i + 1][k + 1]

            // for-loop을 이용하기
            // var numNeighbour = 0
            // for (var m = -1; m <= 1; m++) {
            //     for (var n = -1; n <= 1; n++) {
            //         numNeighbour = numNeighbour + cells[i + m][k + n]
            //     }
            // }

            var newState = rule(cells[i][k], numNeighbour)
            next[i][k] = newState

        }
    }

    display()

    // 다음 세대가 현 세대가 된다.
    cells = next

}

function display() {

    var color = []

    for (var m = 0; m < cells.length; m++) {
        for (var n = 0; n < cells[m].length; n++) {

            if (cells[m][n] == 1) {
                color = [0, 0, 0]
            } else {
                color = [255, 255, 255]
            }

            outlet(0, 'paintrect',
                gridWidth * m, // left
                gridHeight * n, // top
                gridWidth * (m + 1), // right
                gridHeight * (n + 1), // bottom
                color
            )
        }
    }

}
