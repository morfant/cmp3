autowatch = 1
inlets = 1
outlets = 6


var cells = []; // Array
var w = 40
var h = 40
var width, height
var column, row // number of cells
var rand_max = 0.6

// colors
var livingCellColor = [0, 0, 0]
var deadCellColor = [255, 255, 255]

var seqBarColorH = [0, 255, 200]
var seqBarColorV = [255, 0, 200]

var playingCellColorH = [0, 205, 105]
var playingCellColorV = [205, 0, 105]

var errorColor = [255, 0, 0]

var idxToNote = [60, 62, 64, 67, 69, 72, 74, 76, 79, 81]
var idxToNoteV = [60, 62, 64, 67, 69, 72, 74, 76, 79, 81]
var seqPosition = 0
var seqPositionV = 0

var seqBarMargin = 0.1

var _toggleH = true
var _toggleV = true

function toggleH(b) {
    _toggleH = b
}

function toggleV(b) {
    _toggleV = b
}

function bang() {

    display()
    if (_toggleH == true) {
        displaySeqBar(false, seqPosition)
        sound(false, seqPosition)
    }
    if (_toggleV == true) {
        displaySeqBar(true, seqPosition / 2)
        sound(true, seqPosition / 2)
    }

    seqPosition++
    if (seqPosition >= width / w) {
        seqPosition = 0
        generate()
    }

}

function msg_float(num) {
    rand_max = num
}

function size(w, h) {
    width = w
    height = h
    post("w / h", width, height, "\n")
}

function setup() {

    max.clearmaxwindow()
    cells = []

    var changeNotes = []
    var changeNotesV = []
    // idxToNote = [60, 62, 64, 67, 69, 72, 74, 76, 79, 81]
    idxToNote = [60, 62, 64, 67, 69, 72, 74, 76, 79, 81]
    for (var i = 0; i < idxToNote.length; i++) {
        changeNotes[i] = idxToNote[i] - 24
    }
    idxToNote = changeNotes.reverse()

    // idxToNoteV = [60, 62, 64, 67, 69, 72, 74, 76, 79, 81, 84, 86, 88, 91, 93, 96, 98, 100, 103, 105]
    idxToNoteV = [60, 62, 64, 67, 69, 72, 74, 76, 79, 81, 84, 86, 88, 91, 93, 96, 98, 100, 103, 105]
    for (var i = 0; i < idxToNoteV.length; i++) {
        changeNotesV[i] = idxToNoteV[i] - 12
    }
    idxToNoteV = changeNotesV


    seqPosition = 0


    if (width && height) {
        column = width / w
        row = height / h
        post(column, row, "\n")
    } else {
        post("set variable \'width\', \'height\' first")
        return
    }

    // init 2D array 
    for (var x = 0; x < column; x++) {
        cells[x] = [] // Array of arrays, make 2D array
        for (var y = 0; y < row; y++) {
            cells[x][y] = Math.round(rrand(0, rand_max))
            // cells[x][y] = Math.round(Math.random())
        }
    }

}


// make sound
function sound(isVertical, pos) {
    var notesV = []
    var notesH = []

    // post("row: ", row, "\n")
    // post("column: ", column, "\n")
    if (isVertical === true) {
        for (var i = 0; i < column; i++) {
            if (cells[i][pos] === 1) {
                notesV.push(idxToNoteV[i])
                outlet(4, i * w, pos * h, (i + 1) * w, (pos + 1) * h, playingCellColorV)
            }
        }
        outlet(5, notesV)
    } else {
        for (var i = 0; i < row; i++) {
            if (cells[pos][i] === 1) {
                notesH.push(idxToNote[i])
                outlet(4, pos * w, i * h, (pos + 1) * w, (i + 1) * h, playingCellColorH)
            }
        }
        outlet(2, notesH)
    }
}


function displaySeqBar(isVertival, pos) {

    if (isVertival === true) {
        for (var i = 0; i < height; i++) {
            outlet(3, 0, (pos + seqBarMargin) * h, width, (pos + 1 - seqBarMargin) * h, seqBarColorV)
        }
    } else {
        for (var i = 0; i < width; i++) {
            outlet(3, (pos + seqBarMargin) * w, 0, (pos + 1 - seqBarMargin) * w, height, seqBarColorH)
        }
    }
}


function generate() {

    // init 2D array next
    var next = []
    for (var x = 0; x < column; x++) {
        next[x] = [] // Array of arrays, make 2D array
        for (var y = 0; y < row; y++) {
            next[x][y] = 0
        }
    }

    for (var x = 1; x < column - 1; x++) {
        for (var y = 1; y < row - 1; y++) {

            // calculate number of neighbors
            var neighbors = 0

            for (var i = -1; i <= 1; i++) {
                for (var j = -1; j <= 1; j++) {
                    if (cells[x + i][y + j] === 1) {
                        neighbors++
                    }
                }
            }

            // subtract itself from sum
            neighbors -= cells[x][y]

            // apply rule to make next generation
            var newState = rule(cells[x][y], neighbors)
            // console.log(newState)

            next[x][y] = newState

        }
    }

    // make next as current
    cells = next
}

function display() {

    var color = []
    for (var x = 0; x < column; x++) {
        for (var y = 0; y < row; y++) {

            if (cells[x][y] === 1) {
                color = livingCellColor
            } else if (cells[x][y] === 0) {
                color = deadCellColor
            } else {
                color = errorColor
            }

            // It is possible to include type of js array as element of Max list
            // left, top, right, bottom, r, g, b => color
            outlet(0, x * w, y * h, (x + 1) * w, (y + 1) * h, color)

            // Show grid with framerect
            outlet(1, x * w, y * h, (x + 1) * w, (y + 1) * h, 0, 0, 0)

        }
    }

}


function rule(self, neighbors) {

    if (self === 1) {
        if (neighbors <= 1 || neighbors > 3) {
            return 0
        }

    } else if (self === 0) {
        if (neighbors == 3) {
            return 1
        }

    } else {
        // Error check
        return -1
    }

    return self
}


function rrand(min, max) {
    return Math.random() * (max - min) + min; // Math.random returns [0, 1)
}
