autowatch = 1
inlets = 1
outlets = 1

var width = 600 // lcd width
var height = 400 // lcd height

var gridWidth = 10 // 가로 60칸
var color = [0, 0, 0]
var ruleSet = [1, 0, 0, 1, 0, 1, 0, 0] // 000 001 010 011 100 101 110 111
var generation = 0
var current = []
var next = []

var count = 0

// ruleSet[neightbourIdx] ==> 다음 세대의 상태 값
// ruleSet[5] = 1
// ruleSet[2] = 0
// releSet[7] = 1 // ==> 이웃이 100일때의 다음 세대에서의 상태

function bang() {
    generate()
    display()
    generation = generation + 1
}

function setRule(a, b, c, d, e, f, g, h) {
    ruleSet = []
    ruleSet = [a, b, c, d, e, f, g, h]
    post("Ruleset: ", ruleSet, "\n")
}

function setup() {
    // 초기화, 
    // lcd clear - max에서 해도 됨..
    // 변수 초기화 ...

    for (var i = 0; i < width / gridWidth; i++) {
        current[i] = 0
    }

    current[current.length / 2] = 1 // current[30] = 1

    generation = 0

}

function generate() {
    // 이웃들의 상태를 통해서 다음 세대의 내 상태를 결정

    var next = []
    for (var i = 0; i < current.length; i++) { // current와 같은 길이를 가져야 합니다.
        next[i] = 0 // [0, 0, 0, 0, 0, ... 0, 0, 0]
    }


    for (var i = 1; i < current.length - 1; i++) {
        // 이웃들
        var left = current[i - 1] // current[-1]
        var mid = current[i]
        var right = current[i + 1] // current[60]

        // left 0, mid 1, right 0

        var neighbour = "" + left + mid + right

        // 현재 neighbour는 bin. (3bit 010)
        // ruleSet의 index로는 dec가 되어야 한다.
        // 010 ==> 2, 001 ==> 1, 100 ==> 4..

        var neighbourIdx = parseInt(neighbour, 2)
        next[i] = ruleSet[neighbourIdx]
        // post(neighbour)

    }

    current = next

}

function display() {
    // generate()를 통해 결정된 상태를 max 의 lcd 창에 그림 그린다.

    // current
    // [0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, .... 0, 1] 60칸

    // 가로 줄을 한 번에 다 그린다.
    for (var i = 0; i < current.length; i++) {
        if (current[i] == 0) {
            // 검은색
            color = [0, 0, 0]

        } else if (current[i] == 1) {
            // 하얀색
            color = [255, 255, 255]
        }
        outlet(0, "paintrect",
            gridWidth * i,              // left, x
            gridWidth * generation,     // top, y
            gridWidth * (i + 1),        // right, x
            gridWidth * (generation + 1), // bottom, y
            color)
    }

}




