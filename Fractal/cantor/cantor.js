autowatch = 1

var width, height

function size(w, h) {
    width = w
    height = h
}

function cantor(x, y, len) {


    if (len > 2) {
        outlet(0, 'linesegment', x, y, x + len, y)
        y += 40
        cantor(x, y, len / 3)
        cantor(x + (len * 2 / 3), y, len / 3)
    }

}