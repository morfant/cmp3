autowatch = 1


var width, height

function size(w, h) {
    width = w
    height = h
    // post("width: ", width, " / height: ", height)
    // post()
}

function setup() {
    // post("setup()")
    // post()
}

function circles(x, y, rad) {
    // post("circles()", rad)
    // post()

    var l = x - rad
    var r = x + rad
    var t = y - rad
    var b = y + rad
    // var color = [Math.random() * 255, Math.random() * 255, Math.random() * 255]
    var color = [0, 0, 0, 200 / rad * 255]

    outlet(0, "frameoval", l, t, r, b, color)

    rad = rad * 1.03

    if (rad < 200) {
        circles(x, y, rad)
    }
}

function circle4(x, y, rad) {

    var l = x - rad
    var r = x + rad
    var t = y - rad
    var b = y + rad

    // var color = [Math.random() * 255, Math.random() * 255, Math.random() * 255]
    // var color = [255, 255, 255, 120 + (rad / 200 * 135)]
    var color = [255, 255, 255, 140]

    // outlet(0, "frameoval", l, t, r, b, color)
    // outlet(0, "paintoval", l, t, r, b, color)
    outlet(0, "paintrect", l, t, r, b, color)
    // outlet(0, "framerect", l, t, r, b, 100, 10, 250, rad / 200 * 255)

    if (rad > 50) {
        circle4(x - rad, y, rad * 3 / 5)
        circle4(x + rad, y, rad * 3 / 5)
        circle4(x, y - rad, rad * 3 / 5)
        circle4(x, y + rad, rad * 3 / 5)
    }

}