autowatch = 1;

var width = 0;
var height = 0;
var result = 0;
var mode = 0;
var count = 0

function clearConsole() {
  max.clearmaxwindow();
}

function msg_int(n) {
  if (mode === 0) {
    result = factorial(n);
    post("factorial result:(" + n + "): " + result, "\n");
  } else if (mode === 1) {
    result = sigma(n);
    post("sigma result:(" + n + "): " + result, "\n");
  } else if (mode === 2) {
    result = fibo(n);
    post("fibo result:(" + n + "): " + result, "\n");
  } else if (mode === 3) {
    result = fiboCache()(n);
    post("fiboCache result:(" + n + "): " + result, "\n");
  }
}

function setMode(n) {
  mode = n;
}

function factorial(n) {
  if (n > 0) {
    if (n == 1) {
      return 1;
    } else {
      return n * factorial(n - 1);
    }
  } else {
    return 0;
  }
}

function sigma(n) {
  if (n > 0) {
    if (n == 1) {
      return 1;
    } else {
      return n + sigma(n - 1);
    }
  } else {
    return 0;
  }
}

function fibo(n) {
  if (n >= 0) {
    if (n === 0) return 0;
    else if (n === 1) return 1;
    return fibo(n - 1) + fibo(n - 2);
  } else {
    return 0;
  }
}

function fiboCache() {
  var result;
  var cache = { "0": 0, "1": 1 };

  var func = function (n) {
    if (typeof cache[n] === "number") {
      result = cache[n];
    } else {
      result = cache[n] = func(n - 1) + func(n - 2);
    }

    return result;
  };

  return func;
}

function size(w, h) {
  width = w;
  height = h;
  post("width: ", width, "height: ", height, "\n");
}

function drawCircle(x, y, r) {
  var color = [0, 0, 0];

  outlet(0, "frameoval", x - r / 2, y + r / 2, x + r / 2, y - r / 2, color);

  if (r > 4) {
    r = r * 0.98;
    drawCircle(x, y, r);
  }
}

function setup() {
  count = 0
}

function circles(x, y, r) {
  var color = [0, 0, 0];

  outlet(0, "frameoval", x - r / 2, y + r / 2, x + r / 2, y - r / 2, color);
  outlet(0, "moveto", x, y)
  outlet(0, "write", count)
  count++

  if (r > 80) {
    circles(x - r / 2, y, r / 2);
    circles(x + r / 2, y, r / 2);

    circles(x, y - r / 2, r / 2);
    circles(x, y + r / 2, r / 2);
  }
}
