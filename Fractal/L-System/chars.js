autowatch = 1
outlets = 2


var current = "" // initial state, axiom
var axiom = "A"
var count = 0

function bang() {
    sound()
}

function reset() {
    current = axiom
    count = 0
    max.clearmaxwindow()
    post(current)
    post()
}

function generate() {

    // A->AB
    // B->A

    var next = ""

    for (var i = 0; i < current.length; i++) {

        var c = current[i]

        if (c == "A") {
            next = next + "AB"
        } else if (c == "B") {
            next = next + "+A-CBA+B"
        } else if (c == "C") {
            next = next + "C"
        } else {
            next = next + c // bypass
        }

    }

    current = next
    post(current)
    post()

}

function sound() {

    if (count < current.length) {
        var c = current[count]

        if (c == "A") {
            outlet(0, 69)
        } else if (c == "B") {
            outlet(0, 71)
        } else if (c == "C") {
            outlet(0, 60, 60)
        } else if (c == "-") {
            outlet(0, 0, 0)
            outlet(1, 60, 68, 63)
        } else if (c == "+") {
            outlet(0, 60, 100)
            outlet(1, 53)
        }

    }

    count++
    if (count >= current.length) {
        count = 0
    }

}