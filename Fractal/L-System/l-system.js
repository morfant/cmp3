var width, height;
var current = "";
var len = 0.03;
var angle = 20;
var nGen = 5;

sketch.default2d();

function setSize(w, h) {
    width = w;
    height = h;
    post("width: ", width, "height: ", height, "\n");
}

function clear() {
    sketch.glclear();
    refresh();
}

function clearConsole() {
    max.clearmaxwindow();
}

function setup() {
    current = "X"; // Axiom, init sentance

    post("setup(): ", current, "\n");
    post("width: ", width, "height: ", height, "\n");

    sketch.glloadidentity(); // reset opengl matrix

    // caution : gl coordinate x: -1(left) ~ 1(right) / y: -1(bottom) ~ 1(top)
    sketch.gltranslate(0, -1); // set origin as center/bottom
    sketch.moveto(0, 0);
    sketch.gllinewidth(4.0); // thickness of line
    refresh();

    // generate sentances
    for (var i = 0; i < nGen; i++) {
        generate();
    }
}

function generate() {
    // Apply rules
    var next = "";

    for (var i = 0; i < current.length; i++) {
        if (current[i] === "F") {
            // next += "FF++F-F-F--F+F+F";
            next += "FF";
            // next += "F+F-F-FF+F+F-F";
            // next += "FF+[+F-F-F]-[-F+F+F]";
            // next += "F[+F]F[-F]F";
            // next += "FF+[FF]--[FF]";
        } else if (current[i] === "A") {
            next += "AB";
        } else if (current[i] === "B") {
            next += "B";
        } else if (current[i] === "X") {
            next += "F[+X]F[-X]+X";
        } else {
            // if not match anything, just send itself
            next += current[i];
        }
    }

    current = next;
    // post(current, "\n");
}

function display() {
    for (var i = 0; i < current.length; i++) {
        with (sketch) {
            if (current[i] === "F") {
                glcolor(0, 0, 0);
                moveto(0, 0);
                line(0, len);
                gltranslate(0, len);
                refresh();
            } else if (current[i] === "G") {
                moveto(0, len);
                gltranslate(0, len);
            } else if (current[i] === "-") {
                glrotate(angle, 0, 0, 1);
            } else if (current[i] === "+") {
                glrotate(-angle, 0, 0, 1);
            } else if (current[i] === "[") {
                glpushmatrix();
            } else if (current[i] === "]") {
                glpopmatrix();
            }
        }
    }
}

function draw() {
    sketch.glclearcolor(0.2, 0.4, 0.3); // background color (r, g, b, a)
    sketch.glclear();
    sketch.glpushmatrix();
    display();
    sketch.glpopmatrix();
}

function msg_float(f) {
    len = f;
    draw();
}

function msg_int(i) {
    nGen = i;
    setup();
    draw();
}
