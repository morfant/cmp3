autowatch = 1

var width, height
var x = 0, y = 0
var direction = 0, angle = 20
var step = 4
var current = ""

var stack_pos = []
var stack_angle = []

function size(w, h) {
    width = w
    height = h
}

function setup() {

    x = width / 2
    y = height / 2
    // current = "F-F-F-F"
    // current = "e"
    current = "Fl"
    direction = -90
    outlet(0, "clear")

}

function degToRad(deg) {
    return deg / 180 * Math.PI
}

function reset() {
    outlet(0, "clear")
    direction = -90
    x = width / 2
    y = height / 2
}

function bang() {
    generate()
    reset()
    turtle(current)
}

function generate() {
    var next = ""

    for (var i = 0; i < current.length; i++) {

        var c = current[i]

        if (c == "F") {
            // var str_alter = "F-F+F+FF-F-F+F"
            // var str_alter = "FF-F-F-F-F-F+F"
            // var str_alter = "F[+F]F[-F]F"
            var str_alter = "F[+F]F[-F][F]"
            next = next + str_alter
        } else if (c == 'e') { // Fl
            // var str_alter = "E+e+E"
            var str_alter = "e+E++E-e--ee-E+"
            next = next + str_alter
        } else if (c == 'E') { // Fr
            // var str_alter = "e-E-e"
            var str_alter = "-e+EE++E+e--e-E"
            next = next + str_alter
        } else if (c == 'L') {
            var str_alter = "LFLF+RFR+FLFL-FRF-LFL-FR+F+RF-LFL-FRFRFR+"
            next = next + str_alter
        } else if (c == 'R') {
            var str_alter = "-LFLFLF+RFR+FL-F-LF+RFR+FLF+RFRF-LFL-FRFR"
            next = next + str_alter
        } else if (c == 'l') {
            // var str_alter = "l+rF++rF-l--ll-rF+"
            var str_alter = "l+rF+"
            next = next + str_alter
        } else if (c == 'r') {
            // var str_alter = "-Fl+rr++r+Fl--Fl-r"
            var str_alter = "-Fl-r"
            next = next + str_alter
        }
        else {
            // next.push(c)
            next = next + c
        }

    }

    current = next

}


function turtle(str) {

    for (var i = 0; i < str.length; i++) {

        var c = str[i]
        var angleRad = degToRad(direction % 360)

        if (c == 'F') {
            // go and draw a line
            var new_x = x + step * Math.cos(angleRad)
            var new_y = y + step * Math.sin(angleRad)

            outlet(0, "linesegment", x, y, new_x, new_y)

            x = new_x
            y = new_y

        } else if (c == '+') {
            // turn right by angle
            direction = direction + angle

        } else if (c == '-') {
            // turn left by angle
            direction = direction - angle

        } else if (c == 'f') {
            // go without drawing a line
            var new_x = x + step * Math.cos(angleRad)
            var new_y = y + step * Math.sin(angleRad)

            x = new_x
            y = new_y
        } else if (c == 'e') { // Fl
            var new_x = x + step * Math.cos(angleRad)
            var new_y = y + step * Math.sin(angleRad)

            outlet(0, "linesegment", x, y, new_x, new_y)

            x = new_x
            y = new_y

            // direction = direction - angle
            // angleRad = degToRad(direction % 360)

            // new_x = x + step * Math.cos(angleRad)
            // new_y = y + step * Math.sin(angleRad)

            // outlet(0, "linesegment", x, y, new_x, new_y)

            // x = new_x
            // y = new_y

        } else if (c == 'E') {// Fr
            var new_x = x + step * Math.cos(angleRad)
            var new_y = y + step * Math.sin(angleRad)

            outlet(0, "linesegment", x, y, new_x, new_y)

            x = new_x
            y = new_y

            // direction = direction + angle
            // angleRad = degToRad(direction % 360)

            // new_x = x + step * Math.cos(angleRad)
            // new_y = y + step * Math.sin(angleRad)

            // outlet(0, "linesegment", x, y, new_x, new_y)

            // x = new_x
            // y = new_y

        } else if (c == '[') {
            var pos = { x: x, y: y }
            stack_pos.push(pos)
            stack_angle.push(direction)
        } else if (c == ']') {
            var pos = stack_pos.pop()
            x = pos.x
            y = pos.y

            direction = stack_angle.pop()
        }


    }

}


function startTrack() {

}