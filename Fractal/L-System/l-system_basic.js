mgraphics.init()
mgraphics.relative_coords = 1
mgraphics.autofill = 0

autowatch = 1

var width, height
var current = []
var len = 50

function size(w, h) {
    width = w
    height = h
    post("width: ", w, "height: ", h, "\n")
}

function clearConsole() {
    max.clearmaxwindow()
}

function setup() {
    current = ["A"]
}

function generate() {
    var next = []

    for (var i = 0; i < current.length; i++) {
        if (current[i] === "A") {
            next.push("A")
            next.push("B")
        } else if (current[i] === "B") {
            next.push("A")
        } else {
        }
    }

    post(current, "\n")
    current = next
}

function display() {
    if (current[i] === "F") {
        outlet(0, "linesegment", 0, 0, 0, len)
        outlet(0, "moveto", 0, len)
    } else if (current[i] === "G") {
        outlet(0, "moveto", 0, len)
    } else if (current[i] === "+") {
    } else if (current[i] === "-") {
    } else {
    }
}

function main() {
    // generate()
    // display()

    mgraphics.rectangle(-2, 2, 2, -2)
    mgraphics.fill()
}

function paint() {
    with (mgraphics) {
        move_to(-1.0, -1.0) // the bottom-left
        line_to(1.0, 1.0) // the top-right
        stroke() // draw it

        move_to(1.0, -1.0) // the bottom-right
        line_to(-1.0, 1.0) // the top-left
        stroke() // draw it
    }
}

// https://stackoverflow.com/questions/17410809/how-to-calculate-rotation-in-2d-in-javascript
// http://blog.naver.com/PostView.nhn?blogId=dalsapcho&logNo=20144939371&parentCategoryNo=&categoryNo=29&viewDate=&isShowPopularPosts=false&from=postList
/*
nx = x * cos(theta) - y * sin(theta)
ny = x * sin(theta) + y * cos(theta)
*/
function rotate(cx, cy, x, y, angle) {
    var radians = (Math.PI / 180) * angle
    cos = Math.cos(radians)
    sin = Math.sin(radians)
    nx = cos * (x - cx) + sin * (y - cy) + cx
    ny = cos * (y - cy) - sin * (x - cx) + cy
    return { x: nx, y: ny }
}
