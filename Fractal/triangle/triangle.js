autowatch = 1


var x = 200, y = 200

var angle = Math.PI / 3

var len = 20

var depth = 0

function setup() {

    x = 200
    y = 200
    outlet(0, "moveto", x, y)

}

function degToRad(deg) {
    return deg / 180 * Math.PI
}

function triangle() {

    angle = 0

    var nx = x + len * Math.cos(degToRad(angle))
    var ny = y + len * Math.sin(degToRad(angle))

    outlet(0, "linesegment", x, y, nx, ny)

    x = nx
    y = ny

    angle -= 120
    nx = x + len * Math.cos(degToRad(angle))
    ny = y + len * Math.sin(degToRad(angle))

    outlet(0, "linesegment", x, y, nx, ny)
    x = nx
    y = ny

    angle -= 120
    nx = x + len * Math.cos(degToRad(angle))
    ny = y + len * Math.sin(degToRad(angle))

    outlet(0, "linesegment", x, y, nx, ny)
    x = nx
    y = ny

}