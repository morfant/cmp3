autowatch = 1

inlets = 1
outlets = 2

var rectLen = 2
var width, height
var imageData = []
var particles = [],
    numParticles = 5000

var pixelsIsNew = true

function setSize(w, h) {
    width = w
    height = h
    post("width: ", width, "height: ", height, "\n")
}

function clearConsole() {
    max.clearmaxwindow()
}

function setup() {
    clearConsole()
    post("width: ", width, "height: ", height, "\n")

    outlet(1, width / 2 - 10, height / 2 - 10, width / 2 + 10, height / 2 + 10)
    // getImageData(0, 0, width, height)
    // post(imageData)

    makeParticles()
}

function makeParticles() {
    for (var i = 0; i < numParticles; i++) {
        var p = {
            x: Math.random() * width,
            y: Math.random() * height,
            vx: 0,
            vy: 0
        }

        particles.push(p)
    }
}

function update() {
    // getImageData(0, 0, width, height)
    getImageData()

    if (pixelsIsNew) {
        for (var i = 0; i < numParticles; i++) {
            var p = particles[i]
            updateParticle(p)
        }
    }
}

// function getImageData(bx, by, w, h) {
function getImageData() {
    imageData = []
    pixelsIsNew = false
    for (var y = 0; y < height; y++) {
        for (var x = 0; x < width; x++) {
            outlet(0, "getpixel", x, y)
        }
    }

    // post(imageData)
    // post(imageData.length, "\n")
}

function clearImageData() {
    imageData = []
}

function _getpixel(r) {
    // post(r, "\n")
    // post(g, "\n")
    // post(b, "\n")

    imageData.push(r)

    if (imageData.length == 160000) {
        pixelsIsNew = true
    }
}

function postImageData() {
    post("length: " + imageData.length, "\n")
    post(imageData)
}

function rect(cx, cy, w, h) {
    // in pixel
    // var c = sketch.screentoworld(cx, cy)
    // sketch.glrect(
    //     c[0] - w / 2 / width,
    //     c[1] - h / 2 / height,
    //     c[0] + w / 2 / width,
    //     c[1] + h / 2 / height
    // )
}

function updateParticle(p) {
    var x = Math.round(p.x),
        y = Math.round(p.y),
        pixel = imageData[(y * height + x) * 1 + 0]

    // post(pixel, "\n")
    var hit = false

    if (pixel == 0) {
        hit = true
    }

    if (hit) {
        // post("hit!!")
        // var pos = sketch.screentoworld(p.x, p.y)
        // rect(p.x, p.y, 10, 10)
        outlet(
            0,
            "paintoval",
            x - rectLen / 2,
            y - rectLen / 2,
            x + rectLen / 2,
            y + rectLen / 2
        )
        // sketck.glrect(pos[0] - 10 / with, pos[1] - 10 / height, pos[0] + )
        respawn(p)
    } else {
        p.vx += Math.random() * 0.1 - 0.05
        p.vy += Math.random() * 0.1 - 0.05

        p.x += p.vx
        p.y += p.vy

        p.vx *= 0.99
        p.vy *= 0.99

        if (p.x > width) {
            p.x -= width
        } else if (p.x < 0) {
            p.x += width
        }

        if (p.y > height) {
            p.y -= height
        } else if (p.y < 0) {
            p.y += height
        }

        outlet(
            0,
            "paintoval",
            Math.round(p.x) - rectLen / 2,
            Math.round(p.y) - rectLen / 2,
            Math.round(p.x) + rectLen / 2,
            Math.round(p.y) + rectLen / 2
        )
    }
}

function respawn(p) {
    if (Math.random() < 0.5) {
        p.x = Math.random() * width
        p.y = 0
    } else {
        p.x = 0
        p.y = Math.random() * height
    }
}

function display() {}

function draw() {
    // display()
    // clear()
    update()
}

function clear() {
    outlet(0, "clear")
}

function msg_float(f) {
    len = f
    draw()
}

function msg_int(i) {
    nGen = i
    setup()
    draw()
}
