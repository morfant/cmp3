var particles = [],
    numParticles = 10000
var side = 1
var d = 0

function setup() {
    createCanvas(800, 800)
    rectMode(CENTER)

    makeParticles()
    // frameRate(30)
    pixelDensity(4)
    d = pixelDensity()

    background(255)

    fill(0)
    rect(width / 2, height / 2, 4, 4)
}

function draw() {
    // background(255);

    update()
}

function makeParticles() {
    for (var i = 0; i < numParticles; i++) {
        var p = {
            x: Math.random() * width,
            y: Math.random() * height,
            vx: 0,
            vy: 0
        }

        particles.push(p)
    }
}

function update() {
    loadPixels()

    for (var i = 0; i < numParticles; i++) {
        var p = particles[i]
        updateParticle(p)
    }
}

function updateParticle(p) {
    var hit = false
    var x = Math.round(p.x),
        y = Math.round(p.y),
        pixel = pixels[4 * (width * d * (y * d) + x * d)]

    if (pixel == 0) hit = true

    if (hit) {
        // console.log("hit")

        rect(p.x, p.y, side, side)

        respawn(p)
    } else {
        // p.vx += Math.random() * 0.1 - 0.05
        // p.vy += Math.random() * 0.1 - 0.05

        p.vx += random(-0.05, 0.05)
        p.vy += random(-0.05, 0.05)

        p.x += p.vx
        p.y += p.vy

        p.vx *= 0.99
        p.vy *= 0.99

        if (p.x > width) {
            p.x -= width
        } else if (p.x < 0) {
            p.x += width
        }

        if (p.y > height) {
            p.y -= height
        } else if (p.y < 0) {
            p.y += height
        }

        // fill(0, 100)
        // rect(p.x, p.y, side, side)
    }
}

function respawn(p) {
    if (Math.random() < 0.5) {
        p.x = Math.random() * width
        p.y = 0
    } else {
        p.x = 0
        p.y = Math.random() * height
    }
}
