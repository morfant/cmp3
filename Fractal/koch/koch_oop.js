autowatch = 1;

var n = 5;
var width, height;
var lines;
var counter;

// class
function Kochline(a, b) {
    this.start = { x: a.x, y: a.y };
    this.end = { x: b.x, y: b.y };

    Kochline.prototype.display = function() {
        outlet(
            0,
            "linesegment",
            this.start.x,
            this.start.y,
            this.end.x,
            this.end.y
        );
    };

    Kochline.prototype.kochA = function() {
        return this.start;
    };

    Kochline.prototype.kochE = function() {
        return this.end;
    };

    Kochline.prototype.kochB = function() {
        var x = this.start.x + (this.end.x - this.start.x) / 3;
        var y = this.start.y + (this.end.y - this.start.y) / 3;
        return { x: x, y: y };
    };

    Kochline.prototype.kochD = function() {
        var x = this.start.x + ((this.end.x - this.start.x) / 3) * 2;
        var y = this.start.y + ((this.end.y - this.start.y) / 3) * 2;
        return { x: x, y: y };
    };

    Kochline.prototype.kochC = function() {
        var bx = this.start.x + (this.end.x - this.start.x) / 3;
        var by = this.start.y + (this.end.y - this.start.y) / 3;
        var ex = this.start.x + ((this.end.x - this.start.x) / 3) * 2;
        var ey = this.start.y + ((this.end.y - this.start.y) / 3) * 2;

        var result = rotate(bx, by, ex, ey, 60);

        return { x: result.x, y: result.y };
    };
}

function size(w, h) {
    width = w;
    height = h;
    post("width: ", w, "height: ", h, "\n");
}

function setup() {
    counter = 0;
    lines = [];

    var start = { x: width / 2 - 200, y: height / 2 - 100 };
    var end = { x: width / 2 + 200, y: height / 2 - 100 };

    var r = { x: end.x - start.x, y: end.y - start.y };

    var a = start;
    var b = end;
    var _c = rotate(b.x, b.y, b.x + r.x, b.y + r.y, -60 * 2);
    var c = { x: _c.x, y: _c.y };

    // triangle shape
    lines.push(new Kochline(a, b));

    // uncomment to make snow flake shape
    // lines.push(new Kochline(b, c))
    // lines.push(new Kochline(c, a))

    // for (var i = 0; i < n; i++) {
    //     generate()
    // }
}

function draw() {
    generate();

    // counter++
    // if (counter > lines.length) counter = 0

    // for (var i = 0; i < counter; i++) {
    //     lines[i].display()
    // }

    for (var i = 0; i < lines.length; i++) {
        lines[i].display();
    }
}

function generate() {
    var next = [];

    for (var i = 0; i < lines.length; i++) {
        var a = lines[i].kochA();
        var b = lines[i].kochB();
        var c = lines[i].kochC();
        var d = lines[i].kochD();
        var e = lines[i].kochE();

        next.push(new Kochline(a, b));
        next.push(new Kochline(b, c));
        next.push(new Kochline(c, d));
        next.push(new Kochline(d, e));
    }

    lines = next;
}

// https://stackoverflow.com/questions/17410809/how-to-calculate-rotation-in-2d-in-javascript
// http://blog.naver.com/PostView.nhn?blogId=dalsapcho&logNo=20144939371&parentCategoryNo=&categoryNo=29&viewDate=&isShowPopularPosts=false&from=postList
/*
nx = x * cos(theta) - y * sin(theta)
ny = x * sin(theta) + y * cos(theta)
*/
function rotate(cx, cy, x, y, angle) {
    var radians = (Math.PI / 180) * angle;
    cos = Math.cos(radians);
    sin = Math.sin(radians);
    nx = cos * (x - cx) + sin * (y - cy) + cx;
    ny = cos * (y - cy) - sin * (x - cx) + cy;
    return { x: nx, y: ny };
}

function bang() {
    post("bang");
}
