autowatch = 1
outlets = 2

var current = "A"
var count = 0
var soundCount = 0

function bang() {
    sound()
}

function reset() {
    current = "A"
    count = 0
    max.clearmaxwindow()
}

function generate() {
    if (count == 0) {
        post(current)
        post()
    } else {
        var next = ""
        for (var i = 0; i < current.length; i++) {
            var alphabet = current[i]
            if (alphabet == "A") {
                next = next + "AB" // A -> AB
            } else if (alphabet == "B") {
                next = next + "AC" // B -> A
            } else if (alphabet == "C") {
                next = next + "B-C"
            } else if (alphabet == "-") {
                next = next + "-"
            }
        }
        post(next)
        post()
        current = next
    }
    count = count + 1
}

function sound() {
    var alphabet = current[soundCount]
    if (alphabet == "A") {
        outlet(0, 69, 60)
    } else if (alphabet == "B") {
        outlet(0, 71, 60)
    } else if (alphabet == "C") {
        outlet(0, 0, 0, 500)
    } else if (alphabet == "-") {
        outlet(1, 65, 69, 72)
    }

    soundCount++ // soundCount = soundCound + 1
    if (soundCount >= current.length) {
        soundCount = 0
    }
}