autowatch = 1

function drawCircle(x, y, radius) {
    var left = x - radius
    var top = y - radius
    var right = x + radius
    var bottom = y + radius
    var color = [255, 255, 255,
        radius / 100 * 255]

    outlet(0, "frameoval",
        left, top, right, bottom, color)

    if (radius > 60) {
        drawCircle(x - radius, y, radius * 0.7)
        drawCircle(x + radius, y, radius * 0.7)
        drawCircle(x, y - radius, radius * 0.7)
        drawCircle(x, y + radius, radius * 0.7)
    }
}


