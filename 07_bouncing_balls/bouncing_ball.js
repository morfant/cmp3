autowatch = 1
outlets = 4

// var x, y // 공의 위치
// var vx, vy // 공의 속도
// var new_x, new_y // 공의 새로운 위치
// var r = 10 // 공의 크기(반지름)

var ballColor = [0, 0, 0]
var width, height

// var ball1, ball2, ball3
var nBall = 200
var balls = []

function size(w, h) {
    width = w
    height = h
}

function setup() {
    balls = []
    // x = r + Math.random() * (width - (r * 2))
    // y = r + Math.random() * (height - (r * 2))
    // vx = (Math.random() - 0.5) * 5
    // vy = (Math.random() - 0.5) * 5
    // r = Math.random() * 30

    // balls[0] = new Ball(100, 100, 10, 5, 20)
    // balls[1] = new Ball(200, 10, 10, 10, 2)
    // balls[2] = new Ball(200, 100, 10, 10, 2)

    for (var i = 0; i < nBall; i++) {
        var x = 100 + Math.random() * 200
        var y = 100 + Math.random() * 200
        var vx = (Math.random() - 0.5) * 5
        var vy = (Math.random() - 0.5) * 5
        var r = Math.random() * 30
        var ball = new Ball(x, y, vx, vy, r)
        balls.push(ball)
    }

}

function bang() {

    outlet(0, "clear")
    for (var i = 0; i < balls.length; i++) {
        balls[i].update()
        balls[i].display()
    }

    // update()
    // display()
}

// function update() {

//     new_x = x + vx
//     new_y = y + vy

//     var dur = 100 * (Math.abs(vx) + Math.abs(vy)) // 속도를 이용해서 dur 구하기
//     var velocity = r * 4 // 반지름을 이용해서 velocity 구하기

//     if (new_x + r > width) {
//         vx = vx * -1 // 부호 바꾸기
//         sendNote(true, new_y, velocity, dur)
//     }

//     if (new_x - r < 0) {
//         vx = vx * -1 // 부호 바꾸기
//         sendNote(true, new_y, velocity, dur)
//     }

//     if (new_y + r > height) {
//         vy = vy * -1
//         sendNote(false, new_x, velocity, dur)
//     }

//     if (new_y - r < 0) {
//         vy = vy * -1
//         sendNote(false, new_x, velocity, dur)
//     }

//     x = new_x
//     y = new_y


// }

function display() {

    outlet(0, "clear")
    for (var i = 0; i < balls.length; i++) {
        outlet(0, "paintoval",
            balls[i].x - balls[i].r, balls[i].y - balls[i].r,
            balls[i].x + balls[i].r, balls[i].y + balls[i].r,
            balls[i].color)
    }

}

function sendNote(isVertical, cor, vel, dur) {
    var note, roundNote
    if (isVertical == true) {
        note = (cor / height) * 127
        roundNote = Math.round(note)
    } else {
        note = (cor / width) * 127
        roundNote = Math.round(note)
    }
    outlet(1, roundNote, vel, dur)
}

// class
function Ball(_x, _y, _vx, _vy, _r) {
    this.x = _x
    this.y = _y
    this.vx = _vx
    this.vy = _vy
    this.r = _r
    this.new_x = 0
    this.new_y = 0
    this.color = [0, 0, 0]

    this.update = function () {

        this.new_x = this.x + this.vx
        this.new_y = this.y + this.vy

        var dur = 100 * (Math.abs(this.vx) + Math.abs(this.vy)) // 속도를 이용해서 dur 구하기
        var velocity = this.r * 4 // 반지름을 이용해서 velocity 구하기

        if (this.new_x + this.r > width) {
            this.vx = this.vx * -1 // 부호 바꾸기
            sendNote(true, this.new_y, velocity, dur)
        }

        if (this.new_x - this.r < 0) {
            this.vx = this.vx * -1 // 부호 바꾸기
            sendNote(true, this.new_y, velocity, dur)
        }

        if (this.new_y + this.r > height) {
            this.vy = this.vy * -1
            sendNote(false, this.new_x, velocity, dur)
        }

        if (this.new_y - this.r < 0) {
            this.vy = this.vy * -1
            sendNote(false, this.new_x, velocity, dur)
        }

        this.x = this.new_x
        this.y = this.new_y
    }

    this.display = function () {
        outlet(0, "paintoval",
            this.x - this.r, this.y - this.r,
            this.x + this.r, this.y + this.r,
            this.color)
    }
}


